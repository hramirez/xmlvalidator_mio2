﻿using System;
using System.Linq;
using System.Xml.Schema;
using System.Xml;
using System.IO;
using SW.CFDIValidator.Helpers;

namespace SW.CFDIValidator.XSDValidator
{
    internal class CFDIValidator
    {
        internal ValidateXMLResponse res;
        internal ValidateXMLResponse XMLValidate(string xml)
        {
            res = new ValidateXMLResponse();
            try
            {
                res.cfdi = Serializer.DeserializeObject<Comprobante>(xml);
                res = ValidateInvoiceRemoveAddenda(res.cfdi);
            }
            catch
            {
                res.cfdi = null;
                res.AddError(1000, "Estructura CFDI","No fue posible cargar el XML.", "Comprueba que la estructura del XML del comprobante siga los siguientes lineamientos http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
            }
            return res;
        }
        private ValidateXMLResponse ValidateInvoiceRemoveAddenda(Comprobante invoice)
        {
            var addendaElement = new ComprobanteAddenda();
            if (invoice.Addenda != null)
            {
                addendaElement.Any = invoice.Addenda.Any;
                invoice.Addenda = null;
            }
            
            //
            string xmlResult = SerializeFiscalDocument.Invoice(invoice);

            var res = ValidateInvoice(invoice, xmlResult);

            if (addendaElement.Any != null)
            {
                invoice.Addenda = addendaElement;
                xmlResult = SerializeFiscalDocument.Invoice(invoice);
            }

            res.xmlValid = xmlResult;
            return res;
        }
        private ValidateXMLResponse ValidateInvoice(Comprobante invoice, string xml)
        {
            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationEventHandler += new ValidationEventHandler(configCFD_ValidationEventHandler);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.cfdv32));
                if (invoice.Complemento != null && invoice.Complemento.Any != null)
                {
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "tfd:timbrefiscaldigital"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.timbrefiscaldigital));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "divisas:divisas"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.divisas));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "detallista:detallista"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.detallista));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "donat:donatarias"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.donat11));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "leyendasfisc:leyendasfiscales"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.leyendasFisc));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "pfic:pfintegrantecoordinado"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.pfic));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "tpe:turistapasajeroextranjero"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.turistapasajeroextranjero));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "implocal:impuestoslocales"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.implocal));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "ecc:estadodecuentacombustible"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.ecc));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "spei:complemento_spei"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.spei));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "registrofiscal:cfdiregistrofiscal"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.cfdiregistrofiscal));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "nomina:nomina"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.nomina11));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "pagoenespecie:pagoenespecie"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.pagoenespecie));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "vehiculousado:vehiculousado"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.vehiculousado));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "consumodecombustibles:consumodecombustibles"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.consumodecombustibles));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "valesdedespensa:valesdedespensa"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.valesdedespensa));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "aerolineas:aerolineas"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.aerolineas));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "notariospublicos:notariospublicos"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.notariospublicos));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "cce:comercioexterior"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.comercioexterior10));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "destruccion:certificadodedestruccion"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.certificadodedestruccion));
                    }
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "ine:ine"))
                    {
                        settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.ine10));
                    }
                }
                if (invoice.Conceptos != null && invoice.Conceptos.Length > 0)
                {
                    foreach (ComprobanteConcepto concepto in invoice.Conceptos)
                    {
                        if (concepto.ComplementoConcepto != null && concepto.ComplementoConcepto.Any != null)
                        {
                            foreach (XmlElement element in (concepto.ComplementoConcepto.Any))
                            {
                                switch (element.Name.ToLower())
                                {
                                    case "iedu:insteducativas":
                                        if (!settings.Schemas.Contains("http://www.sat.gob.mx/iedu"))
                                        {
                                            settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.iedu));
                                        }
                                        break;

                                    case "terceros:porcuentadeterceros":
                                        if (!settings.Schemas.Contains("http://www.sat.gob.mx/terceros"))
                                        {
                                            settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.terceros11));
                                        }
                                        break;

                                    case "ventavehiculos:ventavehiculos":
                                        if (!settings.Schemas.Contains("http://www.sat.gob.mx/ventavehiculos"))
                                        {
                                            settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.ventavehiculos11));
                                        }
                                        break;
                                    case "aieps:acreditamientoieps":
                                        if (!settings.Schemas.Contains("http://www.sat.gob.mx/acreditamiento"))
                                        {
                                            settings.Schemas.Add(SW.CFDIXSD.CFDI32.XmlSchemaFile.Get(SW.CFDIXSD.CFDI32.XsdEnum.acreditamientoieps10));
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
                settings.Schemas.Compile();
                StringReader stream = new StringReader(xml);
                using (XmlReader cfd = XmlReader.Create(stream, settings))
                {
                    while (cfd.Read()) ;
                }
            }

            catch (Exception)
            {

                res.AddError(1000,"Estructura CFDI", "Factura con una estructura invalida, no fue posible validarla contra los XSD", "Verifica que la realmente se trate de un archivo con formato XML. Más informacion: https://www.w3.org/standards/xml/");
            }
            return res;
        }
        private void configCFD_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
            {
                //res.canContinue = false;
               
                res.AddError(301, "Estructura CFDI", e.Message, "Verifica que la estructura del XML se encuentre acorde con el XSD.");
            }
            else if (e.Severity == XmlSeverityType.Warning)
            {
                res.AddLog(301, "Estructura CFDI", e.Message, "Verifica que la estructura del XML se encuentre acorde con el XSD.");
            }
        }

        //Creo que esto no se ocupa porque se supone que esta clase sólo valida la estructura del CFDI.
        //Esto debe colocarse en validar correspondencia.
        //internal bool ValidateSign(string cadenaOriginal, string sello, X509Certificate2 certificado)
        //{ 
        //     bool bValidacionSello = false; 
        //     try 
        //     { 
        //         byte[] byteCadenaOriginal = Encoding.UTF8.GetBytes(cadenaOriginal); 
        //         byte[] byteSello = Convert.FromBase64String(sello); 
 

        //         if (certificado == null) 
        //         { 
        //             //El certificado proporcionado no es válido. 
        //             return false; 
        //         } 
        //         RSACryptoServiceProvider rsaCryptoServiceProvider = (RSACryptoServiceProvider)certificado.PublicKey.Key; 
 

        //         bValidacionSello = rsaCryptoServiceProvider.VerifyData(byteCadenaOriginal, CryptoConfig.MapNameToOID("SHA1"), byteSello); 
 

        //         if (cadenaOriginal == null || cadenaOriginal == String.Empty) 
        //         { 
        //             return false; 
        //         } 
        //         else if (!bValidacionSello) 
        //         { 
        //             //El sello proporcionado no es válido las cadenas son diferentes. 
        //             return false; 
        //         } 
        //     } 
        //     catch
        //     { 
                
        //         //Error no controlado en el proceso de validacion del sello. 
        //         return false; 
        //     } 
        //     return bValidacionSello; 
        // } 


    }
}

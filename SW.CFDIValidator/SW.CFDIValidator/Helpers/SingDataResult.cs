﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW.CFDIValidator.Helpers
{
    public class SignDataResult
    {
        private string _certificate;
        public string Certificate
        {
            get { return _certificate; }
            set { _certificate = value; }
        }

        private string _digitalSeal;
        public string DigitalSeal
        {
            get { return _digitalSeal; }
            set { _digitalSeal = value; }
        }

        private string _certificateNumber;
        public string CertificateNumber
        {
            get { return _certificateNumber; }
            set { _certificateNumber = value; }
        }

        private string _version;
        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        private string _cadena;
        public string CadenaOriginal
        {
            get { return _cadena; }
            set { _cadena = value; }
        }
        private bool _isSingValid;
        public bool IsSingValid
        {
            get { return _isSingValid; }
            set { _isSingValid = value; }
        }

        private string error;
        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }
}

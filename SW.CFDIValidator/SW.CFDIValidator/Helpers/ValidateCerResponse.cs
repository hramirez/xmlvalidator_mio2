﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW.CFDIValidator.Helpers
{
    public class ValidateCerResponse : BLBaseClass
    {
        public byte[] PFX { get; set; }
        public bool isDemo { get; set; }
        public bool isFiel { get; set; }
        public string serialNumber { get; set; }
        public DateTime? notBefore { get; set; }
        public DateTime? notAfter { get; set; }
        public string subjectName { get; set; }
        public string issuerName { get; set; }
        public string certificateb64 { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW.CFDIValidator.Helpers
{
    public class BLBaseClass
    {
        StringBuilder errorStringBuilder = new StringBuilder();
        StringBuilder warningStringBuilder = new StringBuilder();
        internal AddLog logMessage = new AddLog();
        public bool hasError { get; set; }
        //public Dictionary<int, string>[] errorDetail = new Dictionary<int, string>[] { new Dictionary<int, string>() };


        //public List<Tuple<int, string, string>> detalleError = new List<Tuple<int, string, string>>();

        public string CodError
        {
            get
            {
                return string.Empty != this.errorStringBuilder.ToString() ? this.errorStringBuilder.ToString().Split('|').Where(w => !string.IsNullOrEmpty(w)).FirstOrDefault<string>() : "";
            }
        }

        public string WarningCodError
        {
            get
            {

                return string.Empty != this.warningStringBuilder.ToString() ? this.warningStringBuilder.ToString().Split('|').Where(w => !string.IsNullOrEmpty(w)).FirstOrDefault<string>() : "";
            }
        }

        //public string ErrorMessage
        //{
        //    get
        //    {

        //        return this.errorStringBuilder.ToString();
        //    }
        //}
        //public string WarningMessage
        //{
        //    get
        //    {

        //        return this.warningStringBuilder.ToString();
        //    }
        //}

        //private bool hasError;

        //public bool HasError
        //{
        //    get {
        //        if (string.Empty!=this.errorStringBuilder.ToString())
        //        {
        //            hasError = true;
        //        }
        //        else
        //        {
        //            hasError = false;
        //        }
        //        return hasError;
        //    }
        //    set {

        //        hasError = value;
        //    }
        //}

        public bool HasError
        {
            get
            {
                return string.Empty != this.errorStringBuilder.ToString();
            }
            //set
            //{
            //    value = string.Empty != this.errorStringBuilder.ToString();
            //}
        }



        //public bool HasWarning
        //{
        //    get
        //    {
        //        return string.Empty != this.warningStringBuilder.ToString();
        //    }
        //}

        public void AddError(int codeError, string validationField, string errorString, string recommenation)
        {
            this.errorStringBuilder.Append('|' + errorString);
            logMessage.AddLogMessage(codeError, validationField, errorString, recommenation, true);
        }

        public void AddLog(int codeError, string validationField, string message, string recommendation)
        {
            logMessage.AddLogMessage(codeError,validationField,message,recommendation,false);
        }
    }

    internal class AddLog
    {
        internal Dictionary<string, List<Log>> ListLog;
        internal AddLog()
        {
            this.ListLog = new Dictionary<string, List<Log>>();
        }
        public void AddLogMessage(int codeError, string validationField, string errorString,string recommendation, bool isError)
        {
            bool existType = false;
            foreach (var item in ListLog.Keys)
            {
                if (item == validationField)
                {
                    existType = true;
                    ListLog[item].Add(new Log(codeError, errorString, recommendation, isError));
                }
            }
            if (!existType)
            {
                ListLog.Add(validationField, new List<Log> { new Log(codeError, errorString, recommendation, isError) });
            }
        }
    }

    public class Log
    {
        public int code { get; }
        public string message { get; }
        public string recommendation { get; }
        public bool isError { get; }
        internal Log(int code, string message,string recommendation, bool isError)
        {
            this.code = code;
            this.message = message;
            this.recommendation = recommendation;
            this.isError = isError;
        }
    }
}

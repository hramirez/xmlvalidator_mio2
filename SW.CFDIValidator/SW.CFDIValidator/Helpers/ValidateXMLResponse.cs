﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW.CFDIValidator.Helpers
{
    public class ValidateXMLResponse : BLBaseClass
    {
        public string xmlValid { get; set; }
        public Comprobante cfdi { get; set; }
        public TimbreFiscalDigital tfd { get; set; }
        //public bool canContinue { get; set; }
        //public ValidateXMLResponse()
        //{
        //    canContinue = true;
        //}

    }
}

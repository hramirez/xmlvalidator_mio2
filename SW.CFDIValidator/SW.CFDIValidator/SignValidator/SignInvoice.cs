﻿using SW.CFDIValidator.CSDValidator;
using SW.CFDIValidator.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SW.CFDIValidator.SignValidator
{
    internal class SignInvoice
    {
        internal static SignDataResult Sign(string originalChain, string password, byte[] PFX)
        {
            var result = new SignDataResult();
            try
            {
                var cryptoService = new CryptographicService();
                var res = cryptoService.EncryptPFX(password, PFX, originalChain);

                result.Certificate = res.Certificate;
                result.DigitalSeal = res.DigitalSeal;
                result.CertificateNumber = res.CertificateNumber;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}

﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SW.CFDIValidator.Helpers;
using SW.CFDIValidator.CSDValidator;

namespace SW.CFDIValidator.SignValidator
{
    class ValidarSello
    {
        private string originalChain;
        public string GetCadenaOriginal
        {
            get { return originalChain; }
        }
        private string GeneratedSign;
        public string GetGeneratedSign
        {
            get { return GeneratedSign; }
        }
        //private byte[] pfx;
        public ValidateXMLResponse resp = new ValidateXMLResponse();
        public ValidarSello()
        {

        }

        internal static bool IsValidCer(string xml)
        {
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            bool isValid = false;
            try
            {
                X509Certificate2 xCertificate = new X509Certificate2(Convert.FromBase64String(invoice.certificado));
                isValid = true;
            }
            catch
            {
                isValid = false;
            }
            return isValid;
        }

        public bool IsValidSign(string originalChain, string sign, X509Certificate2 xCertificate)
        {
            bool isValid = false;
            try
            {
                byte[] byteOriginalChain = Encoding.UTF8.GetBytes(originalChain);
                byte[] byteSign = Convert.FromBase64String(sign);
                if (xCertificate == null)
                {
                    return false;
                }
                RSACryptoServiceProvider rsaCryptoServiceProvider = (RSACryptoServiceProvider)xCertificate.PublicKey.Key;
                isValid = rsaCryptoServiceProvider.VerifyData(byteOriginalChain, CryptoConfig.MapNameToOID("SHA1"), byteSign);
                if (originalChain == null || originalChain == String.Empty)
                {
                    return false;
                }
                else if (!isValid)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                resp.AddError(1000, "Validacion del Sello", "Error en la validacion del sello (" + ex.Message + ")", "Comprueba que el certificado, la estructura del XML o el sello estén bien formados");
            }
            return isValid;
        }

        public string CreateOriginalChain(string xml)
        {
            string originalChain = string.Empty;
            StringWriter write = new StringWriter();
            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(typeof(cadenaoriginal32));
                XmlReader xmlreader = XmlReader.Create(new StringReader(xml));
                xslt.Transform(xmlreader, null, write);
                originalChain = write.ToString().Trim();
                this.originalChain = originalChain;
            }
            catch (Exception ex)
            {
                resp.AddError(1000, "Cadena Original CFDI", "Error en la creacion de la cadena original (" + ex.Message + ")", "Comprueba la estructura del documento del XML");

            }
            finally
            {
                if (write != null)
                {
                    write.Close();
                }
            }
            return originalChain;
        }

        internal string GetOriginalChainTFD(string tfdString)
        {
            string result = string.Empty;
            StringWriter writer = new StringWriter();
            try
            {
                //to replace caracters in original string to HTML caracters               
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(typeof(cadenaoriginalTFD10));
                XmlReader xmlReader = XmlReader.Create(new StringReader(tfdString));
                xslt.Transform(xmlReader, null, writer);
                result = writer.ToString().Trim();
            }
            catch (Exception ex)
            {
                resp.AddError(1000, "Cadena Original TFD", "Error en la creacion de la cadena original (" + ex.Message + ")", "Comprueba la estructura del complemento del tfd (Timbre Fiscal Digital)");
                //throw new Exception("No es posible obtener la cadena original del TimbreFiscal", ex);
            }
            finally
            {
                if (writer != null) writer.Close();
            }

            return result;
        }
        public string GenerateSign(string cadenaOriginal, string password, byte[] PFX)
        {
            string sign = string.Empty;
            try
            {
                var cryptoService = new CryptographicService();
                sign = cryptoService.EncryptPFX(password, PFX, cadenaOriginal).DigitalSeal;
                this.GeneratedSign = sign;
            }
            catch (Exception ex)
            {
                resp.AddError(1000, "Sello", "Error en la generacion del sello (" + ex.Message + ")", "Comprueba que la contraseña, la cadena original, el certificado y la llave sean las adecuadas");
                //Añadir una excepcion.
            }
            return sign;
        }
        public static string GenerateSign2(string cadenaOriginal, string password, byte[] PFX)
        {
            string sign = string.Empty;
            try
            {
                var cryptoService = new CryptographicService();
                sign = cryptoService.EncryptPFX(password, PFX, cadenaOriginal).DigitalSeal;
                //this.GeneratedSign = sign;
            }
            catch 
            {
                //resp.AddError(1000, "Sello", "Error en la generacion del sello (" + ex.Message + ")", "Comprueba que la contraseña, la cadena original, el certificado y la llave sean las adecuadas");
                //Añadir una excepcion.
            }
            return sign;
        }
    }
}

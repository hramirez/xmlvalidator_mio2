﻿using System;
using Org.BouncyCastle.Crypto;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto.Parameters;
using Mono.Security.X509;
using System.Collections;
using SW.CFDIValidator.CSDValidator;

namespace SW.CFDIValidator.SignValidator
{
    internal class MakeCert
    {
        internal static byte[] generatePFX(byte[] bytesCER, byte[] byteKEY, string password)
        {
            try
            {
                char[] arrayOfChars = password.ToCharArray();
                AsymmetricKeyParameter privateKey = Org.BouncyCastle.Security.PrivateKeyFactory.DecryptKey(arrayOfChars, byteKEY);
                RSA subjectKey = DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)privateKey);

                PKCS12 p12 = new PKCS12();
                p12.Password = password;

                ArrayList list = new ArrayList();
                list.Add(new byte[4] { 1, 0, 0, 0 });
                Hashtable attributes = new Hashtable(1);
                attributes.Add(PKCS9.localKeyId, list);
                p12.AddCertificate(new X509Certificate(bytesCER), attributes);
                p12.AddPkcs8ShroudedKeyBag(subjectKey, attributes);
                return p12.GetBytes();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

﻿using SW.CFDIValidator.CSDValidator;
using SW.CFDIValidator.Helpers;
using SW.CFDIValidator.SignValidator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SW.CFDIValidator
{
    /// <summary>
    /// Clase principal que se debe instanciar para realizar las validaciones.
    /// </summary>
    public class SWValidate
    {
        public ValidateXMLResponse respOk;
        /// <summary>
        /// Diccionario de resultados de la validacion estructura.
        /// </summary>
        public ValidateXMLResponse respEstruct;
        /// <summary>
        /// Diccionario de resultados de la validacion Complementos.
        /// </summary>
        public ValidateXMLResponse respCompl;
        /// <summary>
        /// Diccionario de resultados de la validacion Sello CFDI.
        /// </summary>
        public ValidateXMLResponse respCorrespCFDI;
        private ValidateXMLResponse respCSD;
        /// <summary>
        /// Diccionario de resultados de la validacion del certificado emisor.
        /// </summary>
        public ValidateXMLResponse respCerXML;
        /// <summary>
        /// Diccionario de resultados de la validacion Sello TFD.
        /// </summary>
        public ValidateXMLResponse respCorrespTFD;
        /// <summary>
        /// Diccionario de resultados de la validacion del campo sello CFDI contra el campo Timbre Fiscal Digital.
        /// </summary>
        public ValidateXMLResponse respCorrespCFDI_TFD;
        private ValidateCerResponse resultCSDXMLData;
        private Dictionary<string, List<Log>> allLogMessages;
        /// <summary>
        /// Función que regresa todos los status y los resultados en la validación.
        /// </summary>
        public Dictionary<string, List<Log>> AllLogMessages
        {
            get { return allLogMessages; }
            set { allLogMessages = value; }
        }
        /// <summary>
        /// Agrega mensajes al diccionario.
        /// </summary>
        public void AddLogMessage(int codeLog, string validationField, string messageLog, string recommendation, bool isError)
        {
            var log = new ValidateXMLResponse();
            if (isError)
            {
                log.AddError(codeLog, validationField, messageLog, recommendation);
            }
            else
                log.AddLog(codeLog, validationField, messageLog, recommendation);

            allLogMessages = allLogMessages.Union(log.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
        }
        /// <summary>
        /// Realiza la validacion del XML.
        /// </summary>
        public SWValidate(string xml)
        {
            this.respOk = new ValidateXMLResponse();
            this.respEstruct = new ValidateXMLResponse();
            this.respCompl = new ValidateXMLResponse();
            this.respCorrespCFDI = new ValidateXMLResponse();
            this.respCorrespTFD = new ValidateXMLResponse();
            this.respCorrespCFDI_TFD = new ValidateXMLResponse();
            this.resultCSDXMLData = new ValidateCerResponse();
            bool hasTFD = false;
            this.respEstruct = new ValidadorEstructura(xml).MetodoValidacion().resp;

            if (!this.respEstruct.HasError)
            {
                var invoice = Serializer.DeserializeObject<Comprobante>(xml);
                if (invoice.Complemento != null && invoice.Complemento.Any != null)
                {
                    if (invoice.Complemento.Any.ToList().Exists(p => p.Name.ToLower() == "tfd:timbrefiscaldigital"))
                    {
                        hasTFD = true;
                    }
                }
                this.respOk.AddLog(200, "Status", "La estructura del XML esta bien formada.", null);
                this.respEstruct.AddLog(200, "Estructura CFDI", "La estructura de todo el documento XML esta bien de acuerdo a los XSD.", null);
                var validationCERXML = new ValidadorCerXML(xml).MetodoValidacion();
                this.resultCSDXMLData = validationCERXML.csdResult;
                this.respCerXML = validationCERXML.resp;
                if (!this.respCerXML.hasError)
                {
                    this.respOk.AddLog(200, "Status", "El certificado del XML tiene estructura correcta.", null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El issuerName es: " + resultCSDXMLData.issuerName, null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El SerialNumber es: " + resultCSDXMLData.serialNumber, null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El subjectName es: " + resultCSDXMLData.subjectName, null);
                    this.respCompl = new ValidadorComplementos(xml).MetodoValidacion().resp;
                    if (!this.respCompl.hasError)
                    {
                        this.respOk.AddLog(200, "Status", "Los complementos estan bien formados.", null);
                        this.respCorrespCFDI = new ValidadorCorrespondenciaCFDI(xml).MetodoValidacion().resp;
                        if (!this.respCorrespCFDI.HasError)
                        {
                            this.respOk.AddLog(200, "Status", "La correspondencia del sello CFDI es correcta.", null);
                            //Si existe timbre fiscal digital?
                            if (hasTFD)
                            {
                                this.respCorrespCFDI_TFD = new ValidadorCorrespondenciaCFDI_TFD(xml).MetodoValidacion().resp;
                                if (!this.respCorrespCFDI_TFD.HasError)
                                {
                                    this.respOk.AddLog(200, "Status", "La correspondencia entre los sellos CFDI y el CFDI del Timbre Fiscal Digital esta bien.", null);
                                    this.respCorrespTFD = new ValidadorCorrespondenciaTFD(xml).MetodoValidacion().resp;
                                    if (!this.respCorrespTFD.HasError)
                                    {
                                        this.respOk.AddLog(200, "Status", "La correspondencia de sello TFD es correcta.", null);
                                    }
                                    else
                                    {
                                        this.respOk.AddLog(400, "Status", "El sello del TFD no es valido.", "Verifica en la tabla de abajo los detalles de la validación del Sello TFD");
                                    }
                                }
                                else
                                {
                                    this.respOk.AddLog(400, "Status", "No fue posible validar el sello del TFD", "Verifica en la tabla de abajo los detalles de la validacion Sello TFD");
                                }
                            }
                        }
                        else
                        {
                            this.respOk.AddLog(400, "Status", "El sello del CFDI no es valido.", "Verifica en la tabla de abajo los detalles de la validación del Sello CFDI.");
                            if (hasTFD)
                            {
                                this.respOk.AddLog(400, "Status", "No fue posible validar el sello del TFD", "El sello del CFDI debe ser el correcto porque forma parte de la cadena original del TFD.");
                                this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI contra el sello CFDI del campo TimbreFiscalDigital", "El sello del CFDI debe ser valido porque se compara con el sello CFD que se encuentr en el TimbreFiscalDigital.");
                            }
                        }
                    }
                    else
                    {
                        this.respOk.AddLog(400, "Status", "Los complementos estan mal formados.", "Verifica en la tabla de abajo los detalles de la validación de Complementos.");
                        this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI", "Los complementos deben estar bien formados para poder generar la cadena original y validar el sello del CFDI.");
                        if (hasTFD)
                        {
                            this.respOk.AddLog(400, "Status", "No fue posible validar el sello del TFD", "Los complementos deben estar bien formados para poder generar la cadena original del TFD y validar el sello del TFD.");
                            this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI contra el sello CFDI del campo TimbreFiscalDigital", "Los complementos deben estar bien formados para poder validar los sellos del CFD y CFDI.");
                        }
                    }
                }
                else
                {
                    this.respOk.AddLog(400, "Status", "El Certificado del XML está mal.", "Verifica en la tabla de abajo los detalles de la validación del Certificado XML.");
                    this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI.", "El certificado debe estar bien para poder continuar con la validacion del Sello del CFDI.");
                    if (hasTFD)
                    {
                        this.respOk.AddLog(400, "Status", "No fue posible validar el sello del TFD.", "El certificado debe esta bien para poder continuar con la validacion del sello del TFD.");
                        this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI contra el sello CFDI del campo TimbreFiscalDigital.", "Si el certificado esta mal, no es posible generar un sello correcto para validar el sello del CFDI y el sello del CFD del campo TimbreFiscalDigital.");
                    }
                    this.respCompl = new ValidadorComplementos(xml).MetodoValidacion().resp;
                    if (!this.respCompl.hasError)
                    {
                        this.respOk.AddLog(200, "Status", "Los Complementos están bien.", null);
                    }
                    else
                    {
                        this.respOk.AddLog(400, "Status", "Los complementos estan mal formados.", "Verifica en la tabla de abajo los detalles de la validación de Complementos.");
                    }
                }
            }
            else
            {
                var validationCERXML = new ValidadorCerXML(xml).MetodoValidacion();
                this.resultCSDXMLData = validationCERXML.csdResult;
                this.respOk.AddLog(400, "Status", "La estructura esta mal formada.", "Verifica en la tabla de abajo los detalles de la validación de Estructura CFDI.");
                this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI.", "La estructura debe estar bien para generar la cadena original.");
                if (hasTFD)
                {
                    this.respOk.AddLog(400, "Status", "No fue posible validar el sello del TFD.", "Si el sello del CFDI está mal, la cadena original del TFD no podrá ser generada de forma correcta.");
                    this.respOk.AddLog(400, "Status", "No fue posible validar el sello del CFDI contra el sello CFDI del campo TimbreFiscalDigital.", "Si el sello del CFDI está mal formado, no es posible validarlo contra el sello del CFD que se encuentra en el campo tfd.");
                }
                
                this.respCerXML = new ValidadorCerXML(xml).MetodoValidacion().resp;

                if (!this.respCerXML.hasError && this.resultCSDXMLData != null)
                {
                    //if (validationCERXML == null)
                    //{
                    //    this.respOk.AddError(200, "huiashui", "huashuias", "huiashuias");
                    //}
                    this.respOk.AddLog(200, "Status", "El certificado del XML tiene estructura correcta.", null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El issuerName es: " + resultCSDXMLData.issuerName, null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El SerialNumber es: " + resultCSDXMLData.serialNumber, null);
                    this.respCerXML.AddLog(112, "Certificado XML", "El subjectName es: " + resultCSDXMLData.subjectName, null);
                    this.respCompl = new ValidadorComplementos(xml).MetodoValidacion().resp;
                    if (!this.respCompl.hasError)
                    {
                        this.respOk.AddLog(200, "Status", "Los Complementos están bien.", null);
                    }
                    else
                    {
                        //Si los complementos estan mal, ya termina la validacion de todo.
                        this.respOk.AddLog(400, "Status", "Los complementos estan mal formados.", "Verifica en la tabla de abajo los detalles de la validación de Complementos.");
                    }
                }
                else
                {
                    //Si el Certificado del XML esta mal, todavia puede continuar validando los complementos.
                    this.respOk.AddLog(400, "Status", "El Certificado del XML está mal.", "Verifica en la tabla de abajo los detalles de la validación del Certificado XML.");
                    this.respCompl = new ValidadorComplementos(xml).MetodoValidacion().resp;
                    if (!this.respCompl.hasError)
                    {
                        this.respOk.AddLog(200, "Status", "Los Complementos están bien.", null);
                    }
                    else
                    {
                        //Si los complementos estan mal, ya termina la validacion de todo.
                        this.respOk.AddLog(400, "Status", "Los complementos estan mal formados.", "Verifica en la tabla de abajo los detalles de la validación de Complementos.");
                    }
                }
            }
            this.allLogMessages = respOk.logMessage.ListLog.Union(respEstruct.logMessage.ListLog).Union(respCerXML.logMessage.ListLog).Union(respCompl.logMessage.ListLog).Union(respCorrespCFDI.logMessage.ListLog).Union(respCorrespTFD.logMessage.ListLog).Union(respCorrespCFDI_TFD.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
        }

        /// <summary>
        /// Realiza la validación del XML y las credenciales como .CER, .KEY y Password.
        /// Las credenciales las recibe en formato Base64.
        /// </summary>
        /// siedrix
        /// daniel zavala
        public SWValidate(string xml, string cer, string key, string password)
        {
            this.respOk = new ValidateXMLResponse();
            this.respCSD = new ValidateXMLResponse();
            SWValidate validacionXML = new SWValidate(xml);
            var allLogMessagesXML = validacionXML.allLogMessages;
            var datosValidacionCSD = new ValidadorCSD(xml, cer, key, password).MetodoValidacion();
            this.respCSD = datosValidacionCSD.resp;
            if (!respCSD.hasError && !validacionXML.respEstruct.HasError)
            {
                this.respOk.AddLog(200, "Status", "Los archivos certificados estan bien.", null);
                if (validacionXML.respCorrespCFDI.HasError)
                {
                    if (!validacionXML.respCerXML.hasError)
                    {
                        if (datosValidacionCSD.csdResult.certificateb64 == validacionXML.resultCSDXMLData.certificateb64)
                        {
                            byte[] pfx = MakeCert.generatePFX(Convert.FromBase64String(cer), Convert.FromBase64String(key), password);
                            var cryptoService = new CryptographicService();
                            ValidarSello validacion = new ValidarSello();
                            var cadenaOriginal = validacion.CreateOriginalChain(xml);
                            var sign = cryptoService.EncryptPFX(password, pfx, cadenaOriginal).DigitalSeal;
                            this.respOk.AddLog(111, "Sello CFDI", "El sello que debería ser según tus certificados es: " + sign, "Comprueba todos los errores.");
                        }
                        else
                        {
                            this.respOk.AddLog(400, "Status", "El certificado que se muestra en el XML no es igual al archivo certificado (.CER)", "Verifica que se está usando el mismo certificado.");
                            this.respOk.AddError(112, "Sello CFDI", "El certificado del XML no corresponde al archivo (.CER)", "Compara:\n Certificado XML: " + validacionXML.resultCSDXMLData.certificateb64 + "\n Certificado archivo .CER: " + datosValidacionCSD.csdResult.certificateb64);
                            this.respCSD.AddLog(112, "CSD (Archivos .CER .KEY)", "El issuerName del certificado es: " + datosValidacionCSD.csdResult.issuerName, null);
                            this.respCSD.AddLog(112, "CSD (Archivos .CER .KEY)", "El SerialNumber del certificado es: " + datosValidacionCSD.csdResult.serialNumber, null);
                            this.respCSD.AddLog(112, "CSD (Archivos .CER .KEY)", "El subjectName del certificado es: " + datosValidacionCSD.csdResult.subjectName, null);
                        }
                    }
                }
            }
            else
            {
                this.respOk.AddLog(400, "Status", "Los archivos certificados están mal.", "Verifica en la tabla de abajo los detalles de la validación de CSD (Archivos .CER .KEY).");
            }
            this.allLogMessages = allLogMessagesXML.Union(this.respCSD.logMessage.ListLog).Union(this.respOk.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
        }
    }
}

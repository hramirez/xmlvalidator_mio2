﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using SW.CFDIValidator.Helpers;
using SW.CFDIValidator.XSDValidator;
using SW.CFDIValidator.SignValidator;
using SW.CFDIValidator.ExtraValidator;
using SW.CFDIValidator.CSDValidator;
using SW.SATRootCertificate;


namespace SW.CFDIValidator
{
    internal class ValidadorComplementos : Validador
    {
        public ValidadorComplementos(string xml)
        {
            this.xml = xml;
        }
        public override Validacion MetodoValidacion()
        {
            return new ValidacionComplementos(xml);
        }
    }

    internal class ValidadorCSD : Validador
    {
        LoadSATFiles files = LoadSATFiles.LoadFiles();
        private byte[] bCer;
        private byte[] bKey;
        private string rfc;
        private string password;
        public ValidadorCSD(string xml, string bCer, string bKey, string password)
        {
            this.bCer = Convert.FromBase64String(bCer);
            this.bKey = Convert.FromBase64String(bKey);
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            this.rfc = invoice.Emisor.rfc;
            this.password = password;
        }
        public override Validacion MetodoValidacion()
        {
            return new ValidacionCSD(files.GetFiles.GetAllRootCertificates(), bCer, bKey, rfc, password);
        }
    }
    internal class ValidadorCerXML : Validador
    {
        LoadSATFiles files = LoadSATFiles.LoadFiles();
        //private string xml;
        public ValidadorCerXML(string xml)
        {
            this.xml = xml;
        }
        public override Validacion MetodoValidacion()
        {
            return new ValidacionCerXML(files.GetFiles.GetAllRootCertificates(), xml);
        }
    }
    internal class ValidadorCorrespondenciaCerKeyPass : Validador
    {
        public string selloGenerted { get; }
        private string cer;
        private string key;
        private string password;
        public ValidadorCorrespondenciaCerKeyPass(string xml, string cer, string key, string password)
        {
            this.xml = xml;
            this.cer = cer;
            this.key = key;
            this.password = password;

        }
        public override Validacion MetodoValidacion()
        {
            return new ValidacionCorrespondenciaCerKeyPass(xml, cer, key, password);
        }
    }
    internal class ValidadorCorrespondenciaCFDI : Validador
    {
        public ValidadorCorrespondenciaCFDI(string xml)
        {
            this.xml = xml;
        }

        public override Validacion MetodoValidacion()
        {
            return new ValidacionCorrespondenciaCFDI(this.xml);
        }
    }

    internal class ValidadorCorrespondenciaTFD : Validador
    {
        public ValidadorCorrespondenciaTFD(string xml)
        {
            this.xml = xml;
        }

        public override Validacion MetodoValidacion()
        {
            return new ValidacionCorrespondenciaTFD(this.xml);
        }
    }
    internal class ValidadorCorrespondenciaCFDI_TFD : Validador
    {
        public ValidadorCorrespondenciaCFDI_TFD(string xml)
        {
            this.xml = xml;
        }

        public override Validacion MetodoValidacion()
        {
            return new ValidacionCorrespondenciaCFDI_TFD(this.xml);
        }
    }
    internal class ValidadorEstructura : Validador
    {
        public ValidadorEstructura(string xml)
        {
            this.xml = xml;
        }

        public override Validacion MetodoValidacion()
        {
            return new ValidacionEstructura(xml);
        }
    }

    internal abstract class Validador
    {
        protected string xml;
        public abstract Validacion MetodoValidacion();
    }

    internal abstract class Validacion
    {
        public ValidateXMLResponse resp;
        public ValidateCerResponse csdResult;
    }

    internal class ValidacionEstructura : Validacion
    {
        public ValidacionEstructura(string xml)
        {
            SW.CFDIValidator.XSDValidator.CFDIValidator validacion = new SW.CFDIValidator.XSDValidator.CFDIValidator();
            resp = validacion.XMLValidate(xml);
        }
    }

    internal class ValidacionCorrespondenciaCerKeyPass : Validacion
    {
        private string originalChain;
        public string GetOriginalChain
        {
            get { return originalChain; }

        }

        private string generatedSign;
        public string GetGeneratedSign
        {
            get { return generatedSign; }
        }
        private string certificateXML = string.Empty;
        public ValidacionCorrespondenciaCerKeyPass(string xml, string b64Cer, string b64Key, string password)
        {

            resp = new ValidateXMLResponse();
            if (ValidarSello.IsValidCer(xml))
            {
                if (!isValidSign(xml))
                {
                    if (this.certificateXML == b64Cer)
                    {
                        ValidarSello validacion = new ValidarSello();
                        try
                        {
                            var pfx = MakeCert.generatePFX(Convert.FromBase64String(b64Cer), Convert.FromBase64String(b64Key), password);
                            this.generatedSign = validacion.GenerateSign(originalChain, password, pfx);
                        }
                        catch (Exception ex)
                        {
                            resp.AddError(1000, "Generacion del sello del CFDI", "No fue posible obtener el PFX para el sello del CFDI (" + ex.Message + ").", "Verifica que el certificado, la llave y el password sean los correctos y/o no estén corruptos");
                        }

                        resp = validacion.resp;
                        resp.AddError(302, "Sello CFDI", "El sello no corresponde a la cadena original y/o al certificado del CFDI.", null);
                        if (this.generatedSign != string.Empty)
                        {
                            resp.AddLog(110, "Sello CFDI", "Segun tus credenciales, el sello que deberia generar es:  " + this.generatedSign, "Comprueba si se estan usando los archivos .CER, .KEY y password correctos.");
                        }
                        resp.AddLog(108, "Sello CFDI", "La cadena original del XML es: " + this.originalChain, "Comprueba si la cadena original es la correcta a su comprobante.");
                    }
                    else
                    {
                        resp.AddLog(112, "Sello CFDI", "El archivo .cer no es igual al campo certificado del XML.", "Verifica que se esté usando el archivo .CER correcto.");
                    }
                }
            }
            else
            {
                resp.AddError(1000, "Sello CFDI", "El Certificado del emisor del CFDI tiene un formato invalido y el sello no puede ser validado.", "Verifica que el campo certificado del comprobante esté bien formado.");
            }
        }
        private bool isValidSign(string xml)
        {
            bool isValid = false;
            ValidarSello validacion = new ValidarSello();
            this.originalChain = validacion.CreateOriginalChain(xml);

            //this.originalChain = ValidarSello.CreateOriginalChain(xml);
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            var tfd = new TimbreFiscalDigital();
            this.certificateXML = invoice.certificado;
            //this.sign = invoice.sello;
            X509Certificate2 xCertificate = new X509Certificate2(Convert.FromBase64String(invoice.certificado));
            isValid = validacion.IsValidSign(originalChain, invoice.sello, xCertificate);
            return isValid;
        }
    }

    internal class ValidacionCorrespondenciaCFDI : Validacion
    {
        private string originalChain;
        public string GetOriginalChain
        {
            get { return originalChain; }
        }
        private string sign = string.Empty;
        public ValidacionCorrespondenciaCFDI(string xml)
        {
            resp = new ValidateXMLResponse();
            if (ValidarSello.IsValidCer(xml))
            {
                if (!isValidSign(xml))
                {
                    resp.AddError(302, "Sello CFDI", "El sello no corresponde a la cadena original y/o al certificado del CFDI.", "Para mas informacion, puede proporcionar los archivos .cer .key y su password para mostrar cual sello y/o cadena original es el correcto.");
                    resp.AddLog(107, "Sello CFDI", "El sello que se encuentra en el CFDI es: " + this.sign, "Para saber si el sello está mal formado, proporciona los archivos .CER .KEY y el password.");
                    resp.AddLog(108, "Sello CFDI", "La cadena original del CFDI es: " + this.originalChain, "Comprueba si la cadena original de su comprobante es la correcta.");
                }
                else
                {
                    resp.AddLog(200, "Sello CFDI", "El sello sí corresponde conforme a la cadena original y al certificado.", null);
                    resp.AddLog(200, "Sello CFDI", "El sello del CFDI es: " + this.sign, null);
                    resp.AddLog(200, "Sello CFDI", "La cadena original del CFDI es: " + this.originalChain, null);
                }
            }
            else
                resp.AddError(1000, "Certificado XML", "El Certificado del emisor del CFDI no corresponde al formato y el sello no puede ser validado", "Verifica que el campo certificado del comprobante esté bien formado");
        }

        private bool isValidSign(string xml)
        {
            bool isValid = false;
            ValidarSello validacion = new ValidarSello();
            this.originalChain = validacion.CreateOriginalChain(xml);
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            this.sign = invoice.sello;
            X509Certificate2 xCertificate = new X509Certificate2(Convert.FromBase64String(invoice.certificado));
            isValid = validacion.IsValidSign(originalChain, invoice.sello, xCertificate);
            return isValid;
        }
    }
    internal class ValidacionCorrespondenciaTFD : Validacion
    {
        private string sign = string.Empty;
        public ValidacionCorrespondenciaTFD(string xml)
        {
            resp = new ValidateXMLResponse();
            ValidarSello validacion = new ValidarSello();
            bool isValidTFD;
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            try
            {
                //obtengo el objeto del Timbre Fiscal Digital Deserializado
                var tfd = SerializeFiscalDocument.ValidateNamespaceStructTFD(invoice);
                var tfdString = SerializeFiscalDocument.TimbreFiscalDigital(tfd);
                var cadenaOriginalTFD = validacion.GetOriginalChainTFD(tfdString);
                var noCertificado = tfd.noCertificadoSAT;
                var selloPccSAT = tfd.selloSAT;
                isValidTFD = false;
                LoadSATFiles files = LoadSATFiles.LoadFiles();
                try
                {
                    var cerPccSatb64 = files.GetFiles.GetCertPcc(noCertificado);
                    X509Certificate2 xCertificatePcc = new X509Certificate2(Convert.FromBase64String(cerPccSatb64));
                    isValidTFD = validacion.IsValidSign(cadenaOriginalTFD, selloPccSAT, xCertificatePcc);
                }
                catch (Exception)
                {

                    resp.AddError(1000, "Sello TFD", "No fue posible obtener el certificado del proveedor de certificados digitales.", "Verifica que el número "+noCertificado+ " se encuentre en la lista en http://www.sat.gob.mx/informacion_fiscal/factura_electronica/Paginas/proveedores_autorizados_de_certificacion.aspx");
                }



                if (isValidTFD)
                {
                    resp.AddLog(200, "Sello TFD", "El sello del TFD sí es valido. ", null);
                    resp.AddLog(107, "Sello TFD", "El sello del TFD es: " + selloPccSAT, null);
                    resp.AddLog(107, "Sello TFD", "El sello del CFDI dentro del TimbreFiscalDigital es: " + tfd.selloCFD, null);
                    resp.AddLog(108, "Sello TFD", "La cadena original del TFD es: " + cadenaOriginalTFD,null);
                    resp.AddLog(109, "Sello TFD", "El numero del certificado del PAC (Proveedor Autorizado de Certificado) es: " + noCertificado, null);
                }
                else
                {
                    
                    resp.AddError(400, "Sello TFD", "El sello del TFD no es valido", "Comprueba que los sellos CFDI, SAT y la cadena original del TFD sean correctos");
                    resp.AddLog(107, "Sello TFD", "El sello del TFD es: " + selloPccSAT, null);
                    resp.AddLog(107, "Sello TFD", "El sello del CFDI del TFD es: " + tfd.selloCFD, "Comprueba que sea el mismo sello del CFDI.");
                    resp.AddLog(108, "Sello TFD", "La cadena original del TFD es: " + cadenaOriginalTFD, "Comprueba si no existen caracteres raros.");
                    //resp.AddLog(109, "Sello TFD", "El numero del certificado del PAC (Proveedor Autorizado de Certificado) es: " + noCertificado, "Verifique que esté usando el certificado correcto dentro de esta lista: http://www.sat.gob.mx/informacion_fiscal/factura_electronica/Paginas/proveedores_autorizados_de_certificacion.aspx");
                }
            }
            catch (Exception)
            {
                resp.AddError(1000, "Sello TFD", "No fue posible validar el sello del TFD.", "Verifica que el TFD exista en el XML.");
            }
        }
    }
    internal class ValidacionCorrespondenciaCFDI_TFD : Validacion
    {
        private string sign = string.Empty;
        public ValidacionCorrespondenciaCFDI_TFD(string xml)
        {
            resp = new ValidateXMLResponse();
            var invoice = Serializer.DeserializeObject<Comprobante>(xml);
            try
            {
                var tfd = SerializeFiscalDocument.ValidateNamespaceStructTFD(invoice);
                if (invoice.sello == tfd.selloCFD)
                {
                    resp.AddLog(200, "Sello TFD", "El sello del CFDI es igual al sello CFDI del TimbreFiscalDigital.", null);

                }
                else
                {
                    resp.AddError(400, "Sello TFD", "El sello del CFDI no es igual al sello CFD del Timbre Fiscal Digital.", "Es posible que se le haya colocado un TFD que no corresponde a la factura.");
                    resp.AddLog(400, "Sello TFD", "El sello del CFDI es: " + invoice.sello + "\nEl sello del CFDI en el campo TimbreFiscalDigital es: " + tfd.selloCFD, "Comprueba que los datos esten bien y que los sellos se hayan generado de forma correcta");
                }
            }
            catch (Exception)
            {
                resp.AddError(307, "Sello TFD", "El timbre fiscal digital no tiene un formato adecuado","Comprueba el formato");
            }
        }
    }
    internal class ValidacionComplementos : Validacion
    {
        public ValidacionComplementos(string xml)
        {
            ValidaComplementos validacion = new ValidaComplementos();
            resp = validacion.XMLValidateCompl(xml);
        }
    }
    internal class ValidacionCSD : Validacion
    {

        public ValidacionCSD(List<string> files, byte[] bCer, byte[] bKey, string rfc, string password)
        {
            var result = new SW.CFDIValidator.CSDValidator.CSDValidator(files, bCer, rfc, bKey, password);
            resp = result.GetResultCSDValidate();
            csdResult = result.GetCSDData();
        }
    }

    internal class ValidacionCerXML : Validacion
    {
        public ValidacionCerXML(List<string> files, string xml)
        {
            resp = new ValidateXMLResponse();
            Comprobante invoice;
            try
            {
                invoice = Serializer.DeserializeObject<Comprobante>(xml);
                var b64 = Convert.FromBase64String(invoice.certificado);
                var result = new SW.CFDIValidator.CSDValidator.CSDValidator(files, b64);
                resp = result.GetResultCSDValidate();
                csdResult = result.GetCSDData();
            }
            catch
            {
                resp.AddError(1000, "Certificado XML", "No fue posible cargar el XML.", "Comprueba que la estructura del XML del comprobante siga los siguientes lineamientos http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
            }

        }
    }

    public class LoadSATFiles
    {
        private FilesSat _files;
        private static LoadSATFiles _loadSATFiles;
        protected LoadSATFiles()
        {
            this._files = new FilesSat();
        }
        public static LoadSATFiles LoadFiles()
        {
            if (_loadSATFiles == null)
            {
                _loadSATFiles = new LoadSATFiles();
            }
            return _loadSATFiles;
        }
        public FilesSat GetFiles
        {
            get
            {
                return this._files;
            }

        }
    }
}

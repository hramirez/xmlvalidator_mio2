﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class Detallista : ValidaComplementos
    {
        public Detallista(string xml)
        {
            try
            {
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "detallista:detallista")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "contentversion")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.3.1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "documentstatus")
                                    {
                                        if (!ValidaDocumentStatusDetallista(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'ORIGINAL, COPY, REEMPLAZA, DELETE'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "type")
                                    {
                                        if (!ValidaTypeDetallista(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'SimpleInvoiceType'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "documentstructureversion")
                                    {
                                        if (!ValidaDocumentStructureVersionDetallista(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AMC8.1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Detallista", null);
            }
        }
    }
}
﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class ImpLocal : ValidaComplementos
    {
        public ImpLocal(string xml)
        {
            try
            {
                decimal importeRetenciones = 0, importeTraslados = 0, totalRetenciones = 0, totalTraslados = 0;
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "implocal:impuestoslocales")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "totalderetenciones")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            totalRetenciones = Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                    if (cfd.Name.ToLower() == "totaldetraslados")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            totalTraslados = Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "implocal:retencioneslocales")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "implocretenido")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Retención 5 % al millar'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "tasaderetencion")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            importeRetenciones += Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "implocal:trasladoslocales")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "imploctrasladado")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'5 % al millar'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "tasadetraslado")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            importeTraslados += Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (cfd.Name.ToLower() == "implocal:impuestoslocales")
                            {
                                string actual = cfd.Name.ToLower();
                                if (totalTraslados != importeTraslados)
                                {
                                    SuFacturaException(false, Text_Errores("104"), 104, "En el campo implocal:trasladoslocales en el complemento '" + actual + "'", "Revisa tus importes");
                                }
                                else
                                {
                                    SuFacturaException(false, Text_Errores("600"), 600, "Importes Iguales a Totales en implocal:trasladoslocales en el complemento '" + actual + "'", null);
                                }
                                if (totalRetenciones != importeRetenciones)
                                {
                                    SuFacturaException(false, Text_Errores("104"), 104, "En el campo implocal:retencioneslocales en el complemento '" + actual + "'", "Revisa tus importes");
                                }
                                else
                                {
                                    SuFacturaException(false, Text_Errores("600"), 600, "Importes Iguales a Totales en implocal:retencioneslocales en el complemento '" + actual + "'", null);
                                }
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Impuestos Locales", null);
            }
        }
    }
}
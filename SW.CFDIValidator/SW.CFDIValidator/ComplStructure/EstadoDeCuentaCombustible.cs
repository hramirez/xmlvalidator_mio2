﻿using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator.ExtraValidator
{
    internal class EstadoDeCuentaCombustible : ValidaComplementos
    {
        private string xml;

        public EstadoDeCuentaCombustible(string xml)
        {
            try
            {
                decimal importes = 0, total = 0;
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "ecc:estadodecuentacombustible")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "tipooperacion")
                                    {
                                        if (!ValidaTipoOperacion(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'monedero electrónico'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numerodecuenta")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 20, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'03800949426'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "total")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "subtotal")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            total = Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "ecc:conceptoestadodecuentacombustible")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "identificador")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'0001'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "fecha")
                                    {
                                        if (!ValidaFecha(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'2016-04-08T06:02:00'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "rfc")
                                    {
                                        if (!ValidarRFCEstructura(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAA010101AAA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "claveestacion")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 10, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'111'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "cantidad")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "nombrecombustible")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Diesel'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "foliooperacion")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1291398'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "valorunitario")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            importes += Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "ecc:traslado")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "impuesto")
                                    {
                                        if (!ValidaTipoImpuesto(cfd.Value))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'0001'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "tasaocuota")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (cfd.Name.ToLower() == "ecc:estadodecuentacombustible")
                            {
                                string actual = cfd.Name.ToLower();
                                if (total != importes)
                                    SuFacturaException(false, Text_Errores("105"), 105, "Importes Iguales a Totales en Estado Consumo Combustibles", "Revisa tus importes");
                                else
                                    SuFacturaException(false, Text_Errores("600"), 600, "Importes Iguales a Totales en Estado Consumo Combustibles", null);
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Estado De Cuenta Combustible", null);
            }
        }
     }
}
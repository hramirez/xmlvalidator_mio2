﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class TuristaPasajeroExtranjero : ValidaComplementos
    {
        public TuristaPasajeroExtranjero(string xml)
        {
            try
            {
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "tpe:turistapasajeroextranjero")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "fechadetransito")
                                    {
                                        if (!ValidaFecha(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'2016-07-10T17:20:45'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "tipotransito")
                                    {
                                        if (!ValidaTipoTransito(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Arribo o Salida'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "tpe:datostransito")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "via")
                                    {
                                        if (!ValidaVia(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Aérea'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "tipoid")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'832912830-E2341'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numeroid")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'EGA93812273-PLM3821'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "nacionalidad")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'GRECIA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "empresatransporte")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'IBERIA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "idtransporte")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'MX8321/GC9328'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Turista Pasajero Extranjero", null);
            }
        }
    }
}
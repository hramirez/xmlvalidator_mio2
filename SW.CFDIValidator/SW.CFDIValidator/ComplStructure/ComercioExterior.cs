﻿using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator.ExtraValidator
{
    internal class ComercioExterior : ValidaComplementos
    {
        private string xml;

        public ComercioExterior(string xml)
        {
            try
            {
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "cce:comercioexterior")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "tipooperacion")
                                    {
                                        if (!ValidaTipoOperacionExterior(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Z o 2'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "clavedepedimento")
                                    {
                                        if (!ValidaClaveDePedimentoExterior(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'A1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "certificadoorigen")
                                    {
                                        if (!ValidaCertificadoOrigenExterior(cfd.Value.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'01 o 02'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "numcertificadoorigen")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 40, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'12KIU678-TTTT-5555-HTCV-TR54355566'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numeroexportadorconfiable")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 50, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'12KIU678355566'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "subdivision")
                                    {
                                        if (!ValidaSubdivisionExterior(cfd.Value))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1 o 2'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "observaciones")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 300, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Esta es mi observacion....'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if(cfd.Name.ToLower() == "tipocambiousd")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'18.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "total")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }else
                            if (cfd.Name.ToLower() == "cce:emisor")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "curp")
                                    {
                                        if (!ValidarCURPEstructura(cfd.Value, actual, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101HNLRNL09'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }else
                            if (cfd.Name.ToLower() == "cce:receptor")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "curp")
                                    {
                                        if (!ValidarCURPEstructura(cfd.Value, actual, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101HNLRNL09'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "numregidtrib")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 40, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101HNLRNL09'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "cce:domicilio")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "calle")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 150, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Av. Chapultepec'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "noexterior")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 55, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'904'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "nointerior")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 30, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'3-A'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "colonia")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Av. Chapultepec'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "localidad")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Ejido La Siembra'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "referencia")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Esquina con Insurgentes'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "municipio")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Monterrey'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "estado")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 2, "exact", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Nuevo Leon'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "pais")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 3, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Mexico'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "codigopostal")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 5, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'6400'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "cce:destinatario")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "numregidtrib")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 40, "max", cfd.Name.ToLower(),false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'A5433fFC'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "rfc")
                                    {
                                        if (!ValidarRFCEstructura(cfd.Value, actual,false))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101AAA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "curp")
                                    {
                                        if (!ValidarCURPEstructura(cfd.Value, actual, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101HNLRNL09'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "nombre")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 300, "max", cfd.Name.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Juan Perez Hernandez'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "cce:descripcionesespecificas")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "marca")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 35, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Sony'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "modelo")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 80, "max", cfd.Name.ToLower(),false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'FX 3323'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "submodelo")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 50, "max", cfd.Name.ToLower(), false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Camara'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numeroserie")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 40, "max", cfd.Name.ToLower(), false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'985445678'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "cce:mercancia")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "noIdentificacion")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'7654345'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "fraccionarancelaria")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 8, "max", cfd.Name.ToLower(),false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'5224566'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "cantidadaduana")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 3, 3, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'34.00'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "unidadAduana")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 2, "max", cfd.Name.ToLower(), false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'01011001'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "valorunitarioaduana")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(),2,0,false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'5999.50'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "valordolares")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'299.00'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Renovacion y Sustitucion Vehiculos", null);
            }
        }

        internal bool ValidaSubdivisionExterior(string value)
        {
            if (ValidaDosDecimas(value, "subdivicion - Comercio Exterior", 0))
            {
                int va = Convert.ToInt32(value);
                return (va == 1 || va == 2);
            } else
                return false;
        }

        private bool ValidaCertificadoOrigenExterior(string value)
        {
            return (value == "01" || value == "02");
        }

        internal bool ValidaClaveDePedimentoExterior(string value)
        {
            return (value == "a1");
        }

        internal bool ValidaTipoOperacionExterior(string value)
        {
            return (value == "z" || value == "2");
        }
    }
}
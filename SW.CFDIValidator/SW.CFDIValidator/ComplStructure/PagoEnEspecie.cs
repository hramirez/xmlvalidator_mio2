﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class PagoEnEspecie : ValidaComplementos
    {
        public PagoEnEspecie(string xml)
        {
            try
            {
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "pagoenespecie:pagoenespecie")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "cvepic")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 25, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'A&amp;C8317286A1-18000101-020'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                        //revisar regex
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "foliosoldon")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 11, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'PE-53-78436'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "pzaartnombre")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'El David");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "pzaarttecn")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Esculpido en marmol a cincel'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "pzaartaprod")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 4, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1501'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "pzaartdim")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 1, "min", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'5.17 metros de altura y 5,572 kilogramos de masa'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Pago En Especie", null);
            }
        }
    }
}
﻿using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator.ExtraValidator
{
    internal class INE : ValidaComplementos
    {
        private string xml;

        public INE(string xml)
        {
            try
            {
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "ine:ine")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.1'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "tipoproceso")
                                    {
                                        if (!ValidaTipoProcesoIne(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Ordinario o Precampaña o Campaña'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "tipocomite")
                                    {
                                        if (!ValidaTipoComiteIne(cfd.Value.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Ordinario o Precampaña o Campaña'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "Idcontabilidad")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 6, "max", cfd.Name.ToLower(),false))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'863567'");
                                        else
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }else
                            if (cfd.Name.ToLower() == "ine:contabilidad")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "Idcontabilidad")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 6, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'863567'");
                                        else
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }else
                            if (cfd.Name.ToLower() == "ine:entidad")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "claveentidad")
                                    {
                                        if (!ValidaFormato(cfd.Value, "min", 1, null, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'32'");
                                        else
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }else
                                    if (cfd.Name.ToLower() == "ambito")
                                    {
                                        if (!ValidaTipoAmbitoIne(cfd.Value.ToLower()))
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'Local'");
                                        else
                                            SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "INE", null);
            }
        }

        internal bool ValidaTipoAmbitoIne(string value)
        {
            return (value == "local" || value == "federal");
        }

        internal bool ValidaTipoProcesoIne(string value)
        {
            return (value == "Ordinario" || value == "Precampaña" || value == "Campaña");
        }
        internal bool ValidaTipoComiteIne(string value)
        {
            return (value == "Ejecutivo Nacional" || value == "Ejecutivo Estatal" || value == "Directivo Estatal");
        }

    }
}
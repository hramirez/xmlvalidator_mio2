﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class ValesDeDespensa : ValidaComplementos
    {
        public ValesDeDespensa(string xml)
        {
            try
            {
                decimal importes = 0, total = 0;
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "valesdedespensa:valesdedespensa")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "tipooperacion")
                                    {
                                        if (!ValidaTipoOperacion(cfd.Value.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'monedero electrónico'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numerodecuenta")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 20, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'03800949426'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    } else
                                    if (cfd.Name.ToLower() == "total")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            total = Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "valesdedespensa:concepto")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "identificador")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 20, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'3054003000160212'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "fecha")
                                    {
                                        if (!ValidaFecha(cfd.Value, cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'2016-07-08T17:02:57'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "rfc")
                                    {
                                        if (!ValidarRFCEstructura(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101AAA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "curp")
                                    {
                                        if (!ValidarCURPEstructura(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'AAAA010101HCLJND07'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "nombre")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 100, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'JORGE VIGIL ESCANDÓN'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "numseguridadsocial")
                                    {
                                        if (!ValidaFormato(cfd.Value, "num", 15, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'91234567890'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    else
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            importes += Convert.ToDecimal(cfd.Value);
                                    }
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (cfd.Name.ToLower() == "valesdedespensa:valesdedespensa")
                            {
                                string actual = cfd.Name.ToLower();
                                if (total != importes)
                                    SuFacturaException(false, Text_Errores("105"), 105, "Importes Iguales a Totales en Vales de despensa", "Revisa tus importes");
                                else
                                    SuFacturaException(false, Text_Errores("600"), 600, "Importes Iguales a Totales en vales de despensa", null);
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Vales De Despensa", null);
            }
        }
    }
}
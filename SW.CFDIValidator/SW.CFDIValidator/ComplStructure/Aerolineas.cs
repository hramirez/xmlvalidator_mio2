﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;

namespace SW.CFDIValidator
{
    internal class Aerolineas : ValidaComplementos
    {
        public Aerolineas(string xml)
        {
            try
            {
                decimal importes = 0, total = 0;
                XmlTextReader cfd = new XmlTextReader(new StringReader(xml));
                while (cfd.Read())
                {
                    switch (cfd.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (cfd.Name.ToLower() == "aerolineas:aerolineas")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "version")
                                    {
                                        if (!ValidaVersion(cfd.Value, actual))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1.0'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                    if (cfd.Name.ToLower() == "tua")
                                    {
                                        if (!ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "aerolineas:otroscargos")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "totalcargos")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            total = Convert.ToDecimal(cfd.Value);
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'1234.10'");
                                    }
                                }
                            }
                            if (cfd.Name.ToLower() == "aerolineas:cargo")
                            {
                                string actual = cfd.Name.ToLower();
                                while (cfd.MoveToNextAttribute())
                                {
                                    if (cfd.Name.ToLower() == "importe")
                                    {
                                        if (ValidaDosDecimas(cfd.Value, cfd.Name.ToLower(), 6))
                                            importes += Convert.ToDecimal(cfd.Value);
                                    }
                                    if (cfd.Name.ToLower() == "codigocargo")
                                    {
                                        if (!ValidaFormato(cfd.Value, "alfa", 8, "max", cfd.Name.ToLower()))
                                            SuFacturaException(true, Text_Errores("301"), 301, UpperTitle(cfd.Name + " en el complemento '" + actual), Text_Errores("604") + "'BA'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(cfd.Name + " en el complemento '" + actual), null);
                                    }
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (cfd.Name.ToLower() == "aerolineas:aerolineas")
                            {
                                string actual = cfd.Name.ToLower();
                                if (total != importes)
                                    SuFacturaException(false, Text_Errores("105"), 105, "Importes Iguales a Totales en Aerolineas", "Revisa tus importes");
                                else
                                    SuFacturaException(false, Text_Errores("600"), 600, "Importes Iguales a Totales en Aerolineas", null);
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Aerolineas", null);
            }
        }
    }
}
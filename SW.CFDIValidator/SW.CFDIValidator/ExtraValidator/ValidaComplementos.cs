﻿using Newtonsoft.Json;
using SW.CFDIValidator.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;

namespace SW.CFDIValidator.ExtraValidator
{

    public class ValidaComplementos
    {
        internal ValidateXMLResponse res;
        internal Dictionary<string, List<Log>> temp;
        public ValidaComplementos()
        {
            if (res == null)
                res = new ValidateXMLResponse();
        }
        public ValidateXMLResponse XMLValidateCompl(string xml)
        {
            res = ValidateCompl_(xml);
            
            return res;
        }

        private ValidateXMLResponse ValidateCompl_(string xml)
        {
            res = new ValidateXMLResponse();
            ValidaComplementos validacion;
            XmlTextReader reader = new XmlTextReader(new StringReader(xml));
            try
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name.ToLower() == "cfdi:comprobante")
                            {
                                validacion = new ValidateComprobante(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "detallista:detallista")
                            {
                                validacion = new Detallista(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "divisas:divisas")
                            {
                                validacion = new Divisas(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "valesdedespensa:valesdedespensa")
                            {
                                validacion = new ValesDeDespensa(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "donat:donatarias")
                            {
                                validacion = new Donatarias(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "aerolineas:aerolineas")
                            {
                                validacion = new Aerolineas(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "consumodecombustibles:consumodecombustibles")
                            {
                                validacion = new ConsumoDeCombustibles(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "implocal:impuestoslocales")
                            {
                                validacion = new ImpLocal(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "leyendasfisc:leyendasfiscales")
                            {
                                validacion = new LeyendasFisc(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "nomina:nomina")
                            {
                                validacion = new Nomina(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "notariospublicos:notariospublicos")
                            {
                                validacion = new NotariosPublicos(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "pagoenespecie:pagoenespecie")
                            {
                                validacion = new PagoEnEspecie(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "pfic:pfintegrantecoordinado")
                            {
                                validacion = new PersonaFisica(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "spei:spei_tercero")
                            {
                                validacion = new SpeiTercero(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "tpe:turistapasajeroextranjero")
                            {
                                validacion = new TuristaPasajeroExtranjero(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "vehiculousado:vehiculousado")
                            {
                                validacion = new VehiculoUsado(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "iedu:insteducativas")
                            {
                                validacion = new InstEducativas(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "terceros:porcuentadeterceros")
                            {
                                validacion = new Terceros(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "ventavehiculos:ventavehiculos")
                            {
                                validacion = new VentaVehiculos(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "registrofiscal:cfdiregistrofiscal")
                            {
                                validacion = new RegistroFiscal(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "ecc:estadodecuentacombustible")
                            {
                                validacion = new EstadoDeCuentaCombustible(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "servicioparcial:parcialesconstruccion")
                            {
                                validacion = new ParcialesConstruccion(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "decreto:renovacionysustitucionvehiculos")
                            {
                                validacion = new RenovacionySustitucionVehiculos(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "destruccion:certificadodedestruccion")
                            {
                                validacion = new CertificadodeDestruccion(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "ine:ine")
                            {
                                validacion = new INE(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            else if (reader.Name.ToLower() == "cce:comercioexterior")
                            {
                                validacion = new ComercioExterior(xml);
                                if (!res.hasError)
                                    res.hasError = validacion.res.HasError;
                                res.logMessage.ListLog = res.logMessage.ListLog.Union(validacion.res.logMessage.ListLog).GroupBy(o => o.Key).ToDictionary(o => o.Key, o => o.SelectMany(kvp => kvp.Value).ToList());
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Valida Complementos, " + ex.Message, 1000, "Valida Complementos", null);
            }
            return res;
        }

        internal string Text_Errores(string search)
        {
            string errores = @"{
                      'Errores_list': [
                        {
                          'Id': '1100',
                          'Message': 'Tu version no es 3.2',
                        },
                        {
                          'Id': '201',
                          'Message': 'UUID Cancelado exitosamente',
                        },
                        {
                          'Id': '202',
                          'Message': 'UUID Previamente cancelado',
                        },
                        {
                          'Id': '203',
                          'Message': 'UUID No corresponde el RFC del emisor y de quien solicita la cancelación',
                        },
                        {
                          'Id': '205',
                          'Message': 'UUID No existe',
                        },
                        {
                          'Id': '301',
                          'Message': 'La Estructura No Corresponde',
                        }, 
                        {
                          'Id': '302',
                          'Message': 'Sello mal formado o inválido ',
                        },
                        {
                          'Id': '303',
                          'Message': 'Sello no corresponde a emisor',
                        },
                        {
                          'Id': '304',
                          'Message': 'Certificado revocado o caduco',
                        },
                        {
                          'Id': '305',
                          'Message': 'La fecha de emisión no esta dentro de la vigencia del CSD del Emisor',
                        },
                        {
                          'Id': '306',
                          'Message': 'EL certificado no es de tipo CSD',
                        },
                        {
                          'Id': '307',
                          'Message': 'El CFDI contiene un timbre previo ',
                        },
                        {
                          'Id': '308',
                          'Message': 'Certificado no expedido por el SAT ',
                        },
                        {
                          'Id': '401',
                          'Message': 'Fecha y hora de generacion fuera de rango',
                        },
                        {
                          'Id': '402',
                          'Message': 'RFC del emisor no se encuentra en el régimen de contribuyentes',
                        },
                        {
                          'Id': '403',
                          'Message': 'La fecha de emisión no es posterior al 01 de enero 2012',
                        },
                        {
                          'Id': '101',
                          'Message': 'La fecha no tiene el formato correcto aaaa-mm-ddThh:mm:ss',
                        },
                        {
                          'Id': '102',
                          'Message': 'La serie acepta una cadena de caracteres alfabéticos de 1 a 25 caracteres sin incluir caracteres acentuados',
                        },
                        {
                          'Id': '103',
                          'Message': 'El domicilio no se encuentra registrado',
                        },
                        {
                          'Id': '104',
                          'Message': 'El valor de tus importes no corresponde al subtotal',
                        },
                        {
                          'Id': '105',
                          'Message': 'El valor de tus importes no corresponde al total', 
                        },
                        {
                          'Id': '106',
                          'Message': 'El valor de tus importes no corresponde al total',
                        },
                        {
                          'Id': '600',
                          'Message': 'Este campo tiene la estructura adecuada',
                        },
                        {
                          'Id': '601',
                          'Message': 'Para tener una buena relacion de factura procura llenar todos los campos',
                        },
                        {
                          'Id': '602',
                          'Message': 'Este campo tiene que coincidir con el total del elemento padre',
                        },
                        {
                          'Id': '603',
                          'Message': 'La suma de tus importes coincide correctamente con el total',
                        },
                        {
                          'Id': '604',
                          'Message': 'Este es un ejemplo de lo que deberia tener tu campo: ',
                        }
                      ]
                    }";
            var ErroresListObj = JsonConvert.DeserializeObject<ErroresRootObject>(errores);
            string result = null;

            foreach (var item in ErroresListObj.Errores_list)
                if (search == "all")
                    result += "'" + item.Id + "' " + item.Message;
                else
                    if (item.Id == search)
                    result += item.Message;
            return result;
        }

        internal void SuFacturaException(bool type, string msg, int code, string value, string recommendation, string yes = null)
        {
            if (type == true)
            {
                res.AddError(code,value, msg, recommendation);
            }
            else
            {
                res.AddLog(code, value, msg, recommendation);
            }
        }

        internal bool ValidaSubTotal(decimal subtotal, string xml)
        {
            bool bValida = true;
            XmlTextReader reader = new XmlTextReader(new StringReader(xml));
            decimal importe = 0;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name.ToLower() == "cfdi:concepto")
                        {
                            while (reader.MoveToNextAttribute())
                            {
                                if (reader.Name.ToLower() == "importe")
                                    importe += Convert.ToDecimal(reader.Value);
                            }
                        }
                        break;
                }
            }
            if (!(subtotal == importe))
                bValida = false;
            return bValida;
        }

        internal void ValidaFechaEstructura(string value)
        {
            DateTime valida72Time = DateTime.Now.ToUniversalTime();
            DateTime fechaActualFutura = valida72Time.AddMinutes(65).ToUniversalTime();
            DateTime fechaPasada = valida72Time.AddHours(-72).ToUniversalTime();
            DateTime fecha = DateTime.ParseExact(value, "s",
                                                    System.Globalization.CultureInfo.InvariantCulture);
            if (!(fecha.ToUniversalTime() > fechaActualFutura || fecha.ToUniversalTime() < fechaPasada))
            {
                try
                {
                    DateTime fechaInicio = new DateTime(2011, 1, 1);
                    if (fecha <= fechaInicio)
                        SuFacturaException(false, Text_Errores("403"), 403, "Fecha", "Tienes que estar dentro de la fecha minimma que es '01-01-2011'");
                }
                catch (Exception ex)
                {
                    SuFacturaException(true, "Error no controlado en el proceso ValidaFechaEmisionVigorCFDI. " + ex.Message, 1000, "Fecha", null);
                }
            }
            else
            {
                SuFacturaException(false, Text_Errores("401"), 401, "Fecha", "Recuerda estar dentro del rango para timbrar facturas");
            }
        }

        internal bool ValidarVigenciaComprobante(string value)
        {
            return value == "3.2";
        }

        internal bool ValidaDomicilio(string value, string campo)
        {
            bool bAddress = true;
            try
            {
                string AddressURL = "http://maps.google.com/maps/api/geocode/json?address=" + value;
                var result = new System.Net.WebClient().DownloadString(AddressURL);
                var addressObj = JsonConvert.DeserializeObject<GoogleRootObject>(result);
                if (!(addressObj.status == "OK"))
                    bAddress = false;
            }
            catch (System.Net.WebException)
            {
                SuFacturaException(true, "Por el momento no se puede verificar el domicilio", 1000, campo, null);
                bAddress = false;
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso de ValidaDomicilio "+ex.Message, 1000, campo, null);
                bAddress = false;
            }
            //Console.WriteLine("Domicilio Expedicion: " + bAddress);
            return bAddress;
        }

        internal bool ValidarRFC(string rfc, string cer, string campo)
        {
            bool bValidacionRFCEmisor = false;
            try
            {
                if (ValidarRFCEstructura(rfc, campo))
                {
                    X509Certificate2 certificado = ObtenerCertificado(cer);
                    string RFCCertificado = String.Empty;
                    string RFCComprobante = rfc.ToUpper();
                    //Separamos todos los conceptos dentro del certificado.
                    string[] strConceptosIssuerName = certificado.Subject.Trim().Split(',');
                    foreach (var strTemp in strConceptosIssuerName.ToList())
                    {
                        //res.AddLog(9999, "INFO", strTemp);
                        string[] strConceptoTemp = strTemp.Split('=');
                        if (strConceptoTemp[0].Trim() == "OID.2.5.4.45")
                        {
                            RFCCertificado = strConceptoTemp[1].Trim().Split('/')[0];
                            //Bug Fix replace "
                            RFCCertificado = RFCCertificado.Replace("\"", "");

                            if (RFCCertificado.Trim().ToUpper() == RFCComprobante)
                            {
                                bValidacionRFCEmisor = true;
                                break;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso de ValidarRFC " + campo + ex.Message, 1000, campo, null);
            }
            //Console.WriteLine("ValidarRFCEmisors: \t" + comprobanteObj.Emisor.rfc.ToUpper() + "\t" + bValidacionRFCEmisor);

            return bValidacionRFCEmisor;
        }

        internal bool ValidarRFCEstructura(string rfc, string campo = null, bool _bool = true)
        {
            bool bValidaEstructuraRFC = false;
            try
            {
                RegexOptions regexOptions = RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline;

                //Valores permitos A a Z, minusculas, Ñ y & de 3 a 4 + Fecha + Homoclave 3 caracteres
                Regex rfc1 = new Regex("^[A-Za-zñÑ&]{3}\\d{6}[A-Za-z0-9]{3}$", regexOptions);
                Regex rfc2 = new Regex("^[A-Za-zñÑ]{4}\\d{6}[A-Za-z0-9]{3}$", regexOptions);

                if (rfc1.IsMatch(rfc))
                {
                    DateTime fechaResultado;
                    DateTime.TryParseExact(rfc.Substring(3, 6), "yyMMdd", null, System.Globalization.DateTimeStyles.AdjustToUniversal, out fechaResultado);

                    if (fechaResultado != null)
                        bValidaEstructuraRFC = true;
                }
                else if (rfc2.IsMatch(rfc))
                {
                    DateTime fechaResultado;
                    DateTime.TryParseExact(rfc.Substring(4, 6), "yyMMdd", null, System.Globalization.DateTimeStyles.AdjustToUniversal, out fechaResultado);

                    if (fechaResultado != null)
                        bValidaEstructuraRFC = true;
                }
                if (!bValidaEstructuraRFC)
                    SuFacturaException(_bool, "El campo RFC que contiene '" + MaxLengh(rfc) + "' no cumple con el formato adecuado en el complemento " + campo, 301, campo, "Recuerda que tu RFC tiene que tener la estructura [A-Z,Ñ,&amp;]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?");
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso ValidarEstructuraRFC " + campo + ", " + ex.Message, 1000, campo, null);
            }
            return bValidaEstructuraRFC;
        }

        internal bool ValidaCertificado(string value)
        {
            bool bValidaNoFiel = false;
            try
            {
                X509Certificate2 certificado = ObtenerCertificado(value);
                if (certificado.Subject.Trim().StartsWith("OU="))
                    bValidaNoFiel = true;
            }
            catch (Exception ex)
            {
                bValidaNoFiel = false;
                //SuFacturaException(true, "Error no controlado en el proceso Certificado, " + ex.Message, 1000, "Certificado", null);
            }
            return bValidaNoFiel;
        }

        internal X509Certificate2 ObtenerCertificado(string value)
        {
            X509Certificate2 certificado = new X509Certificate2(Convert.FromBase64String(value));
            return certificado;
        }

        internal bool ValidaVia(string value)
        {
            bool bTipo = false;
            if (value == "aérea" || value == "marítima" || value == "terrestre")
                bTipo = true;
            else
                SuFacturaException(true, "El campo via que contiene '" + MaxLengh(value) + "' no corresponde al establecido", 301, "Via", "Este campo solo puede contener aérea, marítima o terrestre");
            return bTipo;
        }

        internal bool ValidarCURPEstructura(string value, string campo, bool _bool = true)
        {
            bool bValida = true;
            RegexOptions regexOptions = RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline;
            Regex regexItem = new Regex("^[a-zA-Z]{4,4}[0-9]{6}[a-zA-Z]{6,6}[0-9]{2}$", regexOptions);
            bValida = regexItem.IsMatch(value);
            if (!bValida)
                SuFacturaException(_bool, "El campo CURP que contiene '" + MaxLengh(value) + "' no cumple con el formato adecuado en el complemento " + campo, 301, campo, null);
            return bValida;
        }

        internal bool ValidaNivelEducativo(string value)
        {
            bool bnivelEducativo = false;
            if ((value == "preescolar") || (value == "primaria") || (value == "secundaria") || (value == "profesional técnico") || (value == "bachillerato o su equivalente"))
                bnivelEducativo = true;
            else
                SuFacturaException(true, "El campo nivel educativo que contiene '" + MaxLengh(value) + "' no esta de acuerdo a la estructura", 301, "Nivel Educativo", "Este campo solo puede contener, preescolar, primaria, secundaria, profesional tecnico, bachillerato o su equivalente");
            return bnivelEducativo;
        }

        internal bool ValidaTipoTransito(string value)
        {
            bool bTipo = false;
            if (value == "arribo" || value == "salida")
                bTipo = true;
            else
                SuFacturaException(true, "El campo tipo de transito que contiene '" + MaxLengh(value) + "' no corresponde al establecido", 301, "Tipo de transito", "Este campo solo puede contener, arribo o salida");
            return bTipo;
        }

        internal bool ValidaFormato(string value, string tipo, int longitud, string other = null, string campo = null, bool _bool = true)
        {
            bool bValido = false;
            Regex regexItem = null;
            try
            {
                if (tipo == "str")
                {
                    regexItem = new Regex("^[a-zA-Z]*$");
                    bValido = regexItem.IsMatch(value);
                }
                else if (tipo == "num")
                {
                    regexItem = new Regex("^[0-9]*$");
                    bValido = regexItem.IsMatch(value);
                }
                else if (tipo == "exact")
                {
                    bValido = value.Length == longitud;
                }
                else if (tipo == "min")
                {
                    bValido = value.Length >= longitud;
                }
                else if (tipo == "upper")
                {
                    bValido = value == value.ToUpper();
                }
                else if (tipo == "new")
                {
                    regexItem = new Regex(other);
                    bValido = regexItem.IsMatch(value);
                }
                else
                {
                    bValido = true;
                }
                if (other == "min" && bValido == true)
                    bValido = value.Length >= longitud;
                if (other == "max" && bValido == true)
                {
                    bValido = value.Length <= longitud && value.Length > 0;
                    if (!bValido)
                        SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser menor a " + longitud + " caracteres", 301, campo, "Recuerda utilizar la longitud adecuada" );
                }
                if (other == "exact" && bValido == true)
                    bValido = value.Length == longitud;

                if (!bValido && tipo == "min" || !bValido && other == "min")
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' no tiene un minimo de " + longitud + " caracteres", 301, campo, "Recuerda utilizar la longitud adecuada");
                if (!bValido && tipo == "num")
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser numerico", 301, campo, "Recuerda utilizar solo numeros");
                if (!bValido && tipo == "exact" || !bValido && other == "exact")
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser exactamente igual a " + longitud + " caracteres", 301, campo, "Recuerda utilizar la longitud adecuada");
                if (!bValido && tipo == "upper")
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser en mayusculas todos los caracteres", 301, campo, "Recuerda utilizar el formato adecuado");
                if (!bValido && tipo == "new")
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' no corresponde al formato '" + other + "'", 301, campo, "Recuerda utilizar el formato adecuado");
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso Validar Formato, " + campo + ", " + ex.Message, 1000, campo, null);
                bValido = false;
            }

            return bValido;
        }

        internal string MaxLengh(string value)
        {
            if (value.Length > 20)
                value = value.Substring(0, 20) + "...";
            return value;
        }

        internal bool ValidaDosDecimas(string value, string campo, int limite = 2, decimal max = 0, bool _bool = true)
        {
            bool bDecimal = true;
            try
            {
                byte decimals = (byte)((Decimal.GetBits(Convert.ToDecimal(value))[3] >> 16) & 0x7F);
                if (limite == 2 && decimals != limite || limite != 2 && decimals > limite)
                {
                    bDecimal = false;
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser menor a " + limite + " decimas", 301, campo, "Recuerda utilizar el formato adecuado");
                }
                if (max != 0 && decimals > max)
                    SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' tiene que ser menor a " + max + " valores", 301, campo, "Recuerda utilizar el formato adecuado");
            }
            catch (FormatException)
            {
                SuFacturaException(_bool, "El campo " + campo + " que contiene '" + MaxLengh(value) + "' debe ser numerico y con maximo " + limite + " decimas", 301, campo, "Recuerda utilizar el formato adecuado");
                bDecimal = false;
            }
            catch (Exception ex)
            {
                bDecimal = false;
                SuFacturaException(true, "Error no controlado en el proceso Validar Decimas, " + campo + ", " + ex.Message, 1000, campo, "Recuerda utilizar el formato adecuado");
            }
            return bDecimal;
        }

        internal bool ValidaTypeDetallista(string type)
        {
            bool bType = false;
            if (type == "simpleinvoicetype")
                bType = true;
            return bType;
        }

        internal bool ValidaDocumentStatusDetallista(string documentStatus)
        {
            bool bDocumentStatus = false;
            if (documentStatus == "original" || documentStatus == "copy" || documentStatus == "reemplaza" || documentStatus == "delete")
                bDocumentStatus = true;
            return bDocumentStatus;
        }

        internal bool ValidaDocumentStructureVersionDetallista(string documentStructureVersion)
        {
            bool bDocumentStructure = false;
            if (documentStructureVersion == "amc8.1")
                bDocumentStructure = true;
            return bDocumentStructure;
        }

        internal bool ValidaFecha(string fechastring, string campo, string formato = "s", bool _bool = true)
        {
            bool bFecha = false;
            try
            {
                DateTime date = new DateTime();
                bFecha = DateTime.TryParseExact(fechastring, formato, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                if (!bFecha)
                    SuFacturaException(_bool, "El atributo " + campo + " que contiene '" + MaxLengh(fechastring) + "' no tiene el formato adecuado", 301, campo, "Verifica tu XSD y compara tu estructura en este campo");
            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso de Validar Estructura fecha, " + campo + ", " + ex.Message, 1000, campo, null);
            }
            return bFecha;
        }

        internal bool ValidaTipoOperacionDivisas(string tipoOperacion)
        {
            bool bTipoOperacion = false;
            if (tipoOperacion == "compra" || tipoOperacion == "venta")
                bTipoOperacion = true;
            else
                SuFacturaException(true, "El tipo de Operacion en Divisas no corresponde a los establecidos", 301, "Tipo de Operacion - Divisas", "Este campo solo puede contener, compra o venta");
            return bTipoOperacion;
        }

        internal bool ValidaTipoOperacion(string tipoOperacion)
        {
            bool bTipoOperacion = false;
            if (tipoOperacion == "monedero electrónico")
                bTipoOperacion = true;
            return bTipoOperacion;
        }

        internal bool ValidaVersion(string version, string tipo)
        {
            bool bversion = true;
            string Version;
            if (tipo == "detallista:detallista")
                Version = "1.3.1";
            else if (tipo == "nomina:nomina" || tipo == "terceros:porcuentadeterceros" || tipo == "donat:donatarias" || tipo == "ventavehiculos:ventavehiculos" || tipo == "ecc:estadodecuentacombustible" || tipo == "ine:ine")
                Version = "1.1";
            else
                Version = "1.0";
            bversion = version == Version;
            return bversion;
        }

        internal string UpperTitle(string value)
        {
            var textInfo = CultureInfo.InvariantCulture.TextInfo;
            return textInfo.ToTitleCase(value);
        }
        internal bool ValidaTipoDecreto(string value)
        {
            return (value == "01" || value == "02");
        }
        internal bool ValidaTipoImpuesto(string value)
        {
            return (value == "iva" || value == "ieps");
        }
        internal bool ValidaTipoSerieCertificadoDestruccion(string value)
        {
            return (value == "serie a" || value == "serie b" || value == "serie c" || value == "serie d" || value == "serie e");
        }
        internal bool ValidaAnioAuto(string campo)
        {
            bool bValida = false;
            try
            {
                if (Convert.ToInt32(campo) > 1900)
                    bValida = true;
            }
            catch (FormatException)
            {
                SuFacturaException(true, "El campo año que contiene '" + MaxLengh(campo) + "' debe ser numerico y con maximo 4 enteros", 301, "Año - Destruccion", "Recuerda utilizar el formato adecuado");
            }
            catch (Exception ex)
            {
                bValida = false;
            }
            return bValida;
        }
        
    }
    internal class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    internal class Bounds
    {
        public Location northeast { get; set; }
        public Location southwest { get; set; }
    }

    internal class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    internal class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Bounds viewport { get; set; }
    }

    internal class ResultGoogle
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public bool partial_match { get; set; }
        public List<string> types { get; set; }
    }

    internal class GoogleRootObject
    {
        public List<ResultGoogle> results { get; set; }
        public string status { get; set; }
    }

    internal class Errores
    {
        public string Id { get; set; }
        public string Message { get; set; }
    }

    internal class ErroresRootObject
    {
        public List<Errores> Errores_list = new List<Errores>();
    }
}

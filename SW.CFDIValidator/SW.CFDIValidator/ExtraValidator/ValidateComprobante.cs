﻿using SW.CFDIValidator.ExtraValidator;
using System;
using System.IO;
using System.Xml;
using System.Globalization;

namespace SW.CFDIValidator
{
    internal class ValidateComprobante : ValidaComplementos
    {
        public ValidateComprobante(string xml)
        {
            try
            {
                string certificado = null;
                XmlTextReader reader = new XmlTextReader(new StringReader(xml));
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name.ToLower() == "cfdi:comprobante")
                            {
                                while (reader.MoveToNextAttribute())
                                { //crear restriccion por si no esta el campo
                                    if (reader.Name.ToLower() == "version")
                                    {
                                        if (!ValidarVigenciaComprobante(reader.Value))
                                            SuFacturaException(true, Text_Errores("1100"), 1100, "Version del Comprobante", Text_Errores("604") + "'3.2'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Version del Comprobante", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "serie")
                                    {
                                        if (!ValidaFormato(reader.Value, "str", 25, "max", "serie"))
                                            SuFacturaException(true, Text_Errores("102"), 102, "Serie", Text_Errores("604") + "'C'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Serie", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "folio")
                                    {
                                        if (!ValidaFormato(reader.Value, "num", 20, "max", "folio"))
                                            SuFacturaException(true, Text_Errores("301"), 301, "Folio", Text_Errores("604") + "'38'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Folio", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "fecha")
                                    {
                                        if (ValidaFecha(reader.Value, "Fecha - Comprobante"))
                                        {
                                            ValidaFechaEstructura(reader.Value);
                                        }
                                        else
                                            SuFacturaException(true, Text_Errores("301"), 301, "Fecha - Comprobante", Text_Errores("604") + "'2016-04-08T06:02:00'");
                                    }
                                    else
                                    if (reader.Name.ToLower() == "formadepago")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "formadepago"))
                                            SuFacturaException(true, Text_Errores("301"), 301, "Forma de Pago", Text_Errores("604") + "'Parcialidad 1 de 5'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Forma de Pago", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "nocertificado")
                                    {
                                        if (!ValidaFormato(reader.Value, "num", 20, "exact", "nocertifcado"))
                                            SuFacturaException(true, Text_Errores("301"), 301, "Numero de Certificado", Text_Errores("604") + "'MIIEdDCCA1ygAw.....'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Numero de Certificado", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "certificado")
                                    {
                                        if (ValidaCertificado(reader.Value))
                                            certificado = reader.Value;
                                    }
                                    else
                                    if (reader.Name.ToLower() == "condicionesdepago")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "condicionesdepago", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Condiciones de Pago", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Condiciones de Pago", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "subtotal")
                                    {
                                        if (ValidaDosDecimas(reader.Value, "subtotal", 6))
                                        {
                                            if (!ValidaSubTotal(Convert.ToDecimal(reader.Value), xml))
                                                SuFacturaException(false, Text_Errores("104"), 104, "SubTotal - Comprobante", Text_Errores("602"));
                                            else
                                                SuFacturaException(false, Text_Errores("600"), 600, "SubTotal - Comprobante", null);
                                        }
                                        else
                                        {
                                            SuFacturaException(true, Text_Errores("301"), 301, "Subtotal - Comprobante", Text_Errores("604") + "'1234.10'");
                                        }
                                    }
                                    else
                                    if (reader.Name.ToLower() == "descuento")
                                    {
                                        if (!ValidaDosDecimas(reader.Value, "descuento", 6, 0, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Descuento", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Descuento", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "motivodescuento")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "motivodescuento", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Motivo Descuento", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Motivo Descuento", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "tipocambio")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "tipocambio", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Tipo de Cambio", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Tipo de Cambio", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "moneda")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "moneda", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Moneda", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Moneda", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "total")
                                    {
                                        if (!ValidaDosDecimas(reader.Value, reader.Name.ToLower(), 6))
                                            SuFacturaException(true, Text_Errores("301"), 301, "Total - Comprobante", Text_Errores("604") + "'1234.10'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Total - Comprobante", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "tipodecomprobante")
                                    {
                                        if (!(reader.Value == "ingreso" || reader.Value == "egreso" || reader.Value == "traslado"))
                                            SuFacturaException(true, Text_Errores("301"), 301, "Tipo de Comprobante", Text_Errores("604") + "'Ingreso, Egreso o Traslado'");
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Tipo de Comprobante", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "metododepago")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "metododepago", false))
                                            SuFacturaException(false, Text_Errores("301"), 113, "Metodo de Pago", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Metodo de Pago", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "lugarexpedicion")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "lugarexpedicion"))
                                        {
                                            //if (!ValidaDomicilio(reader.Value, "lugarexpedicion")) 
                                            SuFacturaException(true, Text_Errores("301"), 103, "Lugar Expedicion", Text_Errores("604") + "'Monterrey, Nuevo León'");
                                        }
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Lugar Expedicion", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "numctapago")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 4, "min", "numctapago", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Numero de Cuenta de Pago", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Numero de Cuenta de Pago", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "foliofiscalorig")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "foliofiscalorig", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Folio Fiscal Digital", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Folio Fiscal Digital", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "fechafoliofiscalorig")
                                    {
                                        if (!ValidaFecha(reader.Value, "fechafoliofiscalorig", "s", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Fecha Folio Fiscal Original", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Fecha Folio Fiscal Original", null);
                                    }
                                    else
                                    if (reader.Name.ToLower() == "montofoliofiscalorig")
                                    {
                                        if (!ValidaDosDecimas(reader.Value, "montofoliofiscalorig", 6, 0, false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Monto Folio Fiscal Original", Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Monto Folio Fiscal Original", null);
                                    }
                                }
                            }
                            else
                            if (reader.Name.ToLower() == "cfdi:emisor" || reader.Name.ToLower() == "cfdi:receptor")
                            {
                                string actual = reader.Name.ToLower().Replace("cfdi:", "");
                                while (reader.MoveToNextAttribute())
                                {
                                    if (reader.Name.ToLower() == "rfc")
                                    {
                                        if (certificado != null)
                                        {
                                            if (actual == "emisor")
                                            {
                                                if (!ValidarRFC(reader.Value, certificado, actual))
                                                    SuFacturaException(true, Text_Errores("301"), 301, "RFC " + UpperTitle(actual), Text_Errores("604") + "'AAA010101AAA'");
                                                else
                                                    SuFacturaException(false, Text_Errores("600"), 600, "RFC " + UpperTitle(actual), null);
                                            }
                                            else
                                            {
                                                if (!ValidarRFCEstructura(reader.Value, actual))
                                                    SuFacturaException(true, Text_Errores("301"), 301, "RFC " + UpperTitle(actual), Text_Errores("604") + "'AAA010101AAA'");
                                                else
                                                    SuFacturaException(false, Text_Errores("600"), 600, "RFC " + UpperTitle(actual), null);
                                            }
                                        }

                                    }
                                    else
                                    if (reader.Name.ToLower() == "nombre")
                                    {
                                        if (!ValidaFormato(reader.Value, "alfa", 1, "min", "nombre", false))
                                            SuFacturaException(false, Text_Errores("301"), 301, "Nombre " + UpperTitle(actual), Text_Errores("601"));
                                        else
                                            SuFacturaException(false, Text_Errores("600"), 600, "Nombre " + UpperTitle(actual), null);
                                    }
                                }
                            }
                            else
                            if (reader.Name.ToLower() == "cfdi:domiciliofiscal" || reader.Name.ToLower() == "cfdi:domicilio")
                            {
                                string domicilio = "";
                                string campo = reader.Name.ToLower().Replace("cfdi:", "");
                                while (reader.MoveToNextAttribute())
                                {
                                    if (!ValidaFormato(reader.Value, "alfa", 1, "min", reader.Name + " en el campo " + campo, false))
                                        SuFacturaException(false, Text_Errores("301"), 301, UpperTitle(reader.Name + " " + campo), Text_Errores("601"));
                                    else
                                        SuFacturaException(false, Text_Errores("600"), 600, UpperTitle(reader.Name + " " + campo), null);

                                    //domicilio += reader.Value + ", ";
                                }
                                //if (domicilio != "")
                                //    if(!ValidaDomicilio(domicilio.Remove(domicilio.Length -2), reader.Name))
                                //        SuFacturaException(false, Text_Errores("103") + " para "+ campo, 103);
                            }
                            else
                            if (reader.Name.ToLower() == "cfdi:regimenfiscal")
                            {
                                string campo = reader.Name.ToLower().Replace("cfdi:", "");
                                while (reader.MoveToNextAttribute())
                                {
                                    if (!ValidaFormato(reader.Value, "alfa", 1, "min", campo))
                                        SuFacturaException(true, Text_Errores("301"), 301, "Regimen Fiscal", Text_Errores("604") + "'Regimen General de Ley Personas Morales de Prueba'");
                                    else
                                        SuFacturaException(false, Text_Errores("600"), 600, "Regimen Fiscal", null);
                                }
                            }
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                SuFacturaException(true, "Error no controlado en el proceso al comparar el XML en Complementos, " + ex.Message, 1000, "Comprobante", null);
            }
        }
    }
}
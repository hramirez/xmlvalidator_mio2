﻿using System;
using System.Runtime.Caching;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Crypto.Parameters;
using System.Text.RegularExpressions;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using System.Collections.Generic;
using SW.CFDIValidator.Helpers;
using SW.CFDIValidator.SignValidator;

namespace SW.CFDIValidator.CSDValidator
{
    internal class CSDValidator
    {
        private List<string> files;
        private string ACPRUEBAS = @"CN=A\.C\..*DE PRUEBAS";
        static ObjectCache cache = MemoryCache.Default;
        private ValidateCerResponse result;
        public ValidateXMLResponse resp;
        public CSDValidator(List<string> files, byte[] bCER, string rfc, byte[] bKEY, string password)
        {
            this.files = files;
            this.resp = new ValidateXMLResponse();
            this.result = new ValidateCerResponse();
            Certificate(bCER, rfc, bKEY, password);
        }

        public CSDValidator(List<string> files, byte[] cer)
        {
            this.files = files;
            this.result = new ValidateCerResponse();
            this.resp = new ValidateXMLResponse();
            CertificateXML(cer);
        }
        internal ValidateXMLResponse GetResultCSDValidate()
        {
            try
            {
                this.resp.logMessage.ListLog = result.logMessage.ListLog;
                this.resp.hasError = result.HasError;
            }
            catch (Exception)
            {
                //resp.AddError(1000,"Validacion CSD (Certificados de Sello Digital)","Error al obtener resultados de la validacion del Certificado del Sello Digital","Comprueba que los archivos tengan formato adecuado");
            }
            return resp;

        }
        internal ValidateCerResponse GetCSDData()
        {
            return this.result;
        }
        private ValidateCerResponse Certificate(byte[] bCER, string rfc, byte[] bKEY, string password)
        {
            try
            {
                X509Certificate2 certX509 = default(X509Certificate2);
                byte[] pfx = MakeCert.generatePFX(bCER, bKEY, password);
                result.PFX = (byte[])pfx.Clone();
                certX509 = new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet);
                if (null != certX509)
                {
                    X509Certificate2 c = new X509Certificate2(bCER);
                    Org.BouncyCastle.X509.X509Certificate cert = DotNetUtilities.FromX509Certificate(c);
                    RsaKeyParameters publicKey = (RsaKeyParameters)cert.GetPublicKey();
                    AsymmetricKeyParameter asymmetricKeyParameter = PrivateKeyFactory.DecryptKey(password.ToCharArray(), bKEY);
                    RsaPrivateCrtKeyParameters privateKey = (RsaPrivateCrtKeyParameters)asymmetricKeyParameter;
                    if (!(privateKey.Modulus.Equals(publicKey.Modulus)))
                    {
                        result.AddError(306, "CSD (Archivos .CER .KEY)","La llave no corresponde al certificado emisor. ","Verifica que la llave sea del certificado correcto.");
                    }
                    else
                    {
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "La llave coincide con el certificado del emisor. ", null);
                    }
                    var notEmitido = certificateIssuedBySat(bCER);
                    if (!notEmitido)
                        result.AddError(308, "CSD (Archivos .CER .KEY)", "El Certificado no fue emitido por el SAT.", "Comprueba que el certificado fuera creado con los certificados raíz del SAT http://www.sat.gob.mx/informacion_fiscal/factura_electronica/Documents/solcedi/Cert_Prod.zip ");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "El certificado sí fue emitido por el SAT.", null);
                    if ((DateTime.Now > certX509.NotAfter) || (DateTime.Now < certX509.NotBefore))
                        result.AddError(305, "CSD (Archivos .CER .KEY)", "La fecha de emisión no está dentro de la vigencia del CSD del Emisor.","Comprueba que la fecha de emisión del comprobante esta dentro de la vigencia del Certificado del emisor.");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "La fecha de emisión sí está dentro de la vigencia del certificado del emisor.", null);
                    if (this.isFiel(certX509) && rfc.Length == 12)
                        result.AddError(306, "CSD (Archivos .CER .KEY)", "Este Certificado es una FIEL y no es permitido.","Verifica que el certificado no sea FIEL.");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "El Certificado no es una FIEL.", null);
                    if (!certX509.HasPrivateKey)
                        result.AddError(306, "CSD (Archivos .CER .KEY)", "La contraseña y/o llave privada no son validas para el certificado.","Verifica que la llave y la contraseña sean las correctas para sellar la facturta con el certificado dado.");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "La contraseña si es valida para desencriptar la llave.", null);
                    if (!certX509.Subject.ToUpper().Contains(rfc.ToUpper()) || rfc.Length < 12)
                        result.AddError(303, "CSD (Archivos .CER .KEY)", "El certificado no pertenece al contribuyente mostrado en el CFDI","Comprueba si son los archivos .CER y .KEY correctos para el RFC del comprobante.");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "El certificado sí pertenece al RFC del contribuyente.", null);
                    if (Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS))
                        result.AddLog(106, "CSD (Archivos .CER .KEY)", "El certificado es de prueba.", "Este certificado no es posible usarlo para timbrarlo en productivo, pero sí en ambiente de pruebas. http://pruebascfdi.smartweb.com.mx/Timbrado/wsTimbrado.asmx?WSDL");
                    else
                        result.AddLog(200, "CSD (Archivos .CER .KEY)", "El certificado es de producción.", null);

                    result.isFiel = this.isFiel(certX509);
                    result.isDemo = Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS);

                    //Info Certificate
                    result.serialNumber = CertUtil.getCertNumber(certX509);
                    result.notBefore = certX509.NotBefore;
                    result.notAfter = certX509.NotAfter;
                    result.subjectName = certX509.SubjectName != null ? certX509.SubjectName.Name : "";
                    result.issuerName = certX509.IssuerName != null ? certX509.IssuerName.Name : "";
                    result.certificateb64 = Convert.ToBase64String(bCER);
                }
            }
            catch
            {
                result.AddError(302,"CSD (Archivos .CER .KEY)", "La contraseña no es valida para el .KEY","Verifica que la contraseña sea correcta");
            }
            return result;
        }
        /// <summary>
        /// Validacion del Certificado
        /// </summary>
        /// <param name="bCER"></param>
        /// <returns></returns>
        internal ValidateCerResponse CertificateXML(byte[] bCER)
        {
            result = new ValidateCerResponse();
            try
            {
                X509Certificate2 certX509 = default(X509Certificate2);
                certX509 = new X509Certificate2(bCER);
                //result.AddLog(111, "ENTRO", Convert.ToBase64String(bCER));
                if (null != certX509)
                {
                    //Validate if key and cer match pair using BouncyCastle
                    //result.AddLog(111, "ENTRO", Convert.ToBase64String(bCER));
                    X509Certificate2 c = new X509Certificate2(bCER);
                    Org.BouncyCastle.X509.X509Certificate cert = DotNetUtilities.FromX509Certificate(c);
                    RsaKeyParameters publicKey = (RsaKeyParameters)cert.GetPublicKey();

                    var notEmitido = certificateIssuedBySat(bCER);
                    if (!notEmitido)
                        result.AddError(308, "Certificado XML", "El Certificado no fue emitido por el SAT.", "Comprueba que el certificado fuera creado con los certificados padre del SAT");
                    else
                        result.AddLog(200,"Certificado XML","El Certificado sí fue emitido por el SAT",null);
                    if ((DateTime.Now > certX509.NotAfter) || (DateTime.Now < certX509.NotBefore))
                        result.AddError(305, "Certificado XML", "La fecha de emisión no está dentro de la vigencia del CSD del Emisor", "Comprueba que la fecha de emisión del comprobante esta dentro de la vigencia del Certificado del emisor");
                    else
                        result.AddLog(200,"Certificado XML","La fecha de emisión sí está dentro de la vigencia del certificado del emisor",null);
                    //result.AddError(305, "La vigencia del Certificado no es valida.");
                    if (Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS))
                        result.AddLog(106, "Certificado XML", "El certificado es de prueba ", "Este certificado no es posible timbrarlo en productivo, pero sí en ambiente de pruebas. http://pruebascfdi.smartweb.com.mx/Timbrado/wsTimbrado.asmx?WSDL");
                    else
                        result.AddLog(200, "Certificado XML", "El certificado no es de prueba", null);

                    result.isFiel = this.isFiel(certX509);
                    result.isDemo = Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS);

                    //Info Certificate
                    result.serialNumber = CertUtil.getCertNumber(certX509);
                    result.notBefore = certX509.NotBefore;
                    result.notAfter = certX509.NotAfter;
                    result.subjectName = certX509.SubjectName != null ? certX509.SubjectName.Name : "";
                    result.issuerName = certX509.IssuerName != null ? certX509.IssuerName.Name : "";
                    result.certificateb64 = Convert.ToBase64String(bCER);
                }

            }
            catch
            {
                result.AddError(1000, "Certificado XML", "La estructura del certificado del emisor no es la correcta", "Comprueba que el certificado tenga una estructura adecuada y que no esté corrupto");
            }
            return result;
        }
        internal ValidateCerResponse CertificateCER(byte[] bCER)
        {
            result = new ValidateCerResponse();
            try
            {
                X509Certificate2 certX509 = default(X509Certificate2);
                certX509 = new X509Certificate2(bCER);
                //result.AddLog(111, "ENTRO", Convert.ToBase64String(bCER));
                if (null != certX509)
                {
                    //Validate if key and cer match pair using BouncyCastle
                    //result.AddLog(111, "ENTRO", Convert.ToBase64String(bCER));
                    X509Certificate2 c = new X509Certificate2(bCER);
                    Org.BouncyCastle.X509.X509Certificate cert = DotNetUtilities.FromX509Certificate(c);
                    RsaKeyParameters publicKey = (RsaKeyParameters)cert.GetPublicKey();

                    var notEmitido = certificateIssuedBySat(bCER);
                    if (!notEmitido)
                        result.AddError(308, "Certificado (Archivo .CER)", "El Certificado no fue emitido por el SAT.", "Comprueba que el certificado fuera creado con los certificados padre del SAT");
                    if ((DateTime.Now > certX509.NotAfter) || (DateTime.Now < certX509.NotBefore))
                        result.AddError(305, "Certificado (Archivo .CER)", "La fecha de emisión no está dentro de la vigencia del CSD del Emisor", "Comprueba que la fecha de emisión del comprobante esta dentro de la vigencia del Certificado del emisor");
                    //result.AddError(305, "La vigencia del Certificado no es valida.");
                    if (Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS))
                        result.AddLog(106, "Certificado", "El certificado fue emitido por el SAT pero es de prueba ", "Este certificado no es posible timbrarlo en productivo, pero sí en ambiente de pruebas. http://pruebascfdi.smartweb.com.mx/Timbrado/wsTimbrado.asmx?WSDL");

                    result.isFiel = this.isFiel(certX509);
                    result.isDemo = Regex.IsMatch(certX509.Issuer.ToUpper(), ACPRUEBAS);

                    //Info Certificate
                    result.serialNumber = CertUtil.getCertNumber(certX509);
                    result.notBefore = certX509.NotBefore;
                    result.notAfter = certX509.NotAfter;
                    result.subjectName = certX509.SubjectName != null ? certX509.SubjectName.Name : "";
                    result.issuerName = certX509.IssuerName != null ? certX509.IssuerName.Name : "";
                }

            }
            catch (Exception ex)
            {
                result.AddError(1000, "Certificado del emisor", "La estructura del certificado del emisor no es la correcta (" + ex.Message + ")", "Comprueba que el certificado tenga una estructura adecuada y que no esté corrupto");
            }
            return result;
        }
        internal ValidateCerResponse ValidateCertificatePassword(byte[] bCER, string rfc, byte[] bKEY, string password)
        {
            var result = new ValidateCerResponse();

            X509Certificate2 certX509 = default(X509Certificate2);
            try
            {
                //Make the pfx file
                byte[] pfx = MakeCert.generatePFX(bCER, bKEY, password);
                result.PFX = (byte[])pfx.Clone();

                certX509 = new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet);

                if (null != certX509)
                {
                    if (!certX509.HasPrivateKey)
                        result.AddError(306, "Contraseña y/o llave", "La contraseña y/o llave privada no son validas para el certificado", "Verifica que la llave y la contraseña sean las correctas para sellar la facturta con el certificado dado");
                    if (!certX509.Subject.ToUpper().Contains(rfc.ToUpper()) || rfc.Length < 12)
                        result.AddError(303, "RFC", "El certificado no pertenece al contribuyente mostrado en el CFDI", "Comprueba si son los archivos .CER y .KEY correctos para el RFC del comprobante");

                    //Info Certificate
                    result.serialNumber = CertUtil.getCertNumber(certX509);
                    result.notBefore = certX509.NotBefore;
                    result.notAfter = certX509.NotAfter;
                    result.subjectName = certX509.SubjectName != null ? certX509.SubjectName.Name : "";
                    result.issuerName = certX509.IssuerName != null ? certX509.IssuerName.Name : "";
                }
            }
            catch (Exception ex)
            {
                result.AddError(1000, "CSD (Archivos .CER .KEY)", "Los Datos proporcionados son incorrectos y/o La Contraseña es incorrecta ( " + ex.Message + " )", "Comprueba que los archivos no esten corruptos o que la contraseña sea la correcta para los archivos proporcionados");
            }
            return result;
        }

        private bool isFiel(X509Certificate2 certX509)
        {
            X509KeyUsageExtension keyUsageExtension;
            string KeyUsages;
            bool esFiel = false;

            for (int i = 0; i <= certX509.Extensions.Count; i++)
            {
                try
                {
                    keyUsageExtension = certX509.Extensions[i] as X509KeyUsageExtension;
                    if (null != keyUsageExtension)
                    {
                        KeyUsages = keyUsageExtension.KeyUsages.ToString();

                        if (keyUsageExtension.KeyUsages.ToString().Contains("KeyAgreement") || keyUsageExtension.KeyUsages.ToString().Contains("DataEncipherment"))
                        {
                            esFiel = true;
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return esFiel;
        }
        private bool certificateIssuedBySat(byte[] certificateValidate)
        {
            bool validation = false;
            //X509Certificate2 cert = new X509Certificate2(certificateValidate);
            //X509Certificate2Collection collection = new X509Certificate2Collection();
            
            Mono.Security.X509.X509CertificateCollection collection = new Mono.Security.X509.X509CertificateCollection();
            Mono.Security.X509.X509Certificate certt = new Mono.Security.X509.X509Certificate(certificateValidate);
            Mono.Security.X509.X509Chain xchan = new Mono.Security.X509.X509Chain();
            collection = GetCollection();

            xchan.TrustAnchors = collection;
            validation = xchan.Build(certt);

            if (!validation && xchan.Status == Mono.Security.X509.X509ChainStatusFlags.NotTimeNested)
            {
                validation = true;
            }
            return validation;
        }

        private Mono.Security.X509.X509CertificateCollection GetCollection()
        {
            Mono.Security.X509.X509CertificateCollection value = (Mono.Security.X509.X509CertificateCollection)cache["CerRootCollection"];
            if (value == null)
            {
                value = new Mono.Security.X509.X509CertificateCollection();
                foreach (var item in this.files)
                {
                    byte[] caCertificate = Convert.FromBase64String(item);
                    value.Add(new Mono.Security.X509.X509Certificate(caCertificate));
                    if (value.Count > 0)
                    {
                        CacheItemPolicy notExpiringPolicy = new CacheItemPolicy();
                        cache.Set("CerRootCollection", value, notExpiringPolicy);
                    }
                }
            }
            return value;
        }
    }
}

﻿using ProxyService.wsAutenticacion;
using ProxyService.wsTimbrado;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ProxyService
{
    public class Services
    {
        public string Connect(string user, string pass, string endpoint, string otherendpoint, string xml)
        {
            try
            {
                var myBinding = new BasicHttpBinding();
                myBinding.Security.Mode = BasicHttpSecurityMode.Transport;
                myBinding.Security.Mode = BasicHttpSecurityMode.None;
                myBinding.ReceiveTimeout = new TimeSpan(0, 59, 0);
                myBinding.SendTimeout = new TimeSpan(0, 59, 0);
                myBinding.MaxReceivedMessageSize = 2147483647;
                myBinding.MaxReceivedMessageSize = 2147483647;
                myBinding.MaxBufferSize = 2147483647;
                myBinding.MaxBufferPoolSize = 2147483647;
                myBinding.ReaderQuotas = new XmlDictionaryReaderQuotas();
                myBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
                myBinding.ReaderQuotas.MaxArrayLength = 2147483647;
                myBinding.ReaderQuotas.MaxDepth = 2147483647;
                myBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
                myBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
                var myEndpointAddress = new EndpointAddress(endpoint);
                wsAutenticacionSoap Autentica = new wsAutenticacionSoapClient(myBinding, myEndpointAddress);
                myEndpointAddress = new EndpointAddress(otherendpoint);
                wsTimbradoSoap Timbrado = new wsTimbradoSoapClient(myBinding, myEndpointAddress);

                var result = Autentica.AutenticarBasicoAsync(new AutenticarBasicoRequest()
                {
                    Body = new AutenticarBasicoRequestBody()
                    {
                        password = pass,
                        usuario = user
                    }
                });
                result.Wait();

                string Token = result.Result.Body.AutenticarBasicoResult;
                var result2 = Timbrado.TimbrarXML(xml, Token);

                return result2;
            }
            catch (Exception ex)
            {
                return "Error en el servicio " + ex.Message;
            }
        }
        
    }
}

﻿(function () {
    'use strict';

    var serviceId = 'validateFactory';

    angular.module('validateModuleApp').factory(serviceId,
        ['$http', validateFactory]);

    function validateFactory($http) {

        function getPeople() {
            return $http.get('/api/validation');
        }

        var service = {
            getPeople: getPeople
        };

        return service;
    }
})();
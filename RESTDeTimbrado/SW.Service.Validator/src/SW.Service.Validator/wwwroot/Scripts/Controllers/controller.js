﻿(function () {
    'use strict';

    var controllerId = 'validateController';

    angular.module('validateModuleApp').controller(controllerId,
        ['$scope', 'validateFactory', personController]);

    function personController($scope, personFactory) {
        $scope.go = function (json) {
            $scope.$apply(function () {
                $scope.results = json;
            });
        };
    }
})(); 

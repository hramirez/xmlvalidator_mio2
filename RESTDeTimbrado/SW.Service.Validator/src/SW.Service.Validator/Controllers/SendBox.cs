﻿using System.IO;
using System;
using SW.CFDIValidator;
using Microsoft.AspNetCore.Http;

namespace SW.Service.Validator.Controllers
{
    public class SendBox
    {

        internal object Connect(IFormFile Fxml, IFormFile cer = null, IFormFile key = null, string pass = null)
        {
            try
            {
                string xml = Read_File(Fxml, "xml");

                SWValidate pro;
                if (cer != null && key != null && pass != "")
                {
                    pro = new SWValidate(xml, Read_File(cer, "base"), Read_File(key, "base"), pass);
                }
                else
                {
                    pro = new SWValidate(xml);
                }
                if (!xml.ToLower().Contains("tfd:timbrefiscaldigital"))
                {
                    string Timbre = new ProxyService.Services().Connect("demo", "123456789", "http://pruebascfdi.smartweb.com.mx/Autenticacion/wsAutenticacion.asmx",
                                                                                                              "http://pruebascfdi.smartweb.com.mx/Timbrado/wsTimbrado.asmx?WSDL", xml);
                    pro.AddLogMessage(0, "Timbre", Timbre, null, !Timbre.ToLower().Contains("tfd:timbrefiscaldigital"));
                }
                return pro.AllLogMessages;
            }
            catch (Exception ex)
            {
                SWValidate pro = new SWValidate("");
                pro.AllLogMessages.Clear(); 
                pro.AddLogMessage(0, "Error", "Error en la Lectura de los archivos, " + ex,"Contacta a Soporte",true);

                return pro.AllLogMessages;
            }
        }
        private string Read_File(IFormFile files, string type)
        {
            using (var ms = new MemoryStream())
            {
                files.OpenReadStream().CopyTo(ms);
                if (type == "base")
                {
                    byte[] data = ms.ToArray();
                    return System.Convert.ToBase64String(data);
                }
                else
                {
                    ms.Position = 0;
                    var sr = new StreamReader(ms);
                    return sr.ReadToEnd();
                }
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace SW.Service.Validator.Controllers
{
    [Route("api/[controller]")]
    public class ApiValidationController : Controller
    {
        [HttpPost]
        [Authorize]
        public IActionResult Post()
        {
            var xml = Request.Form.Files.GetFile("xml");
            var cer = Request.Form.Files.GetFile("cer");
            var key = Request.Form.Files.GetFile("key");
            string pass = Request.Form["pass"];

            if (cer != null && key != null && pass != "")
            {
                var res = new SendBox().Connect(xml, cer, key, pass);
                return Json(res);
            }
            else
            {
                var res = new SendBox().Connect(xml, null, null, null);
                return Json(res);
            }
        }
    }
}

﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SW.Service.Validator.Controllers
{
    public class ValidationController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            Session();
            return View();
        }

        private async Task Session()
        {
            var principal = new ClaimsPrincipal(
                new ClaimsIdentity(
                    new Claim[] { new Claim(ClaimTypes.NameIdentifier, "cook") },
                    CookieAuthenticationDefaults.AuthenticationScheme
                 )
            );
            var authManager = HttpContext.Authentication;
            await authManager.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
        }
    }
}
